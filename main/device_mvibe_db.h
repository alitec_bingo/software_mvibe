/*
 * device_mvibe_db.h
 *
 *  Created on: 9 sie 2017
 *      Author: admin
 */
#ifndef DEVICE_MVIBE_DB_H_
#define DEVICE_MVIBE_DB_H_

#define DEV_REG_REFRESH			0x35			// change value to write default values of DeviceReg into FRAM

#define ADC_SENS_FLAG			0			//1 - write    0 - read

#define SN_MV240002							// choose card

/* ---------------------------------------------------------------------------------------- */

#ifdef	SN_MV240002


	#define FIRMWARE_VER		22			// wyswietlane 2.2

	#define DEVICE_SN			666
	#define CARD_TYPE			CARD_MVIBE_V3				// card type - mVibe_V2
	#define PROTOCOL_HDR		"protocol_v4.h"

	#define MAIN_WLAN_SSID		"mVIBE"						/**< Destination SSID */
	#define MAIN_WLAN_PSK		"pswdADS1274"				/**< Password for Destination SSID */

	// sensors sensitivities, frequency ranges, 1z H2519 1x H2520
	#define FAB_CH_GAIN_INTX	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }
	#define NOSENSORS			( 9 )
	#define ADC_RANGE_RMS		( 2.000 )											// ~2.0V, needs calibrating
	#define ADC_SENS			( 1518500.06896869 / ADC_RANGE_RMS / 9.81 )			// (2^23-1)*256 / ( sqrt(2) * range_ADC_RMS * 1000 ) [raw/mV] with g -> m/s2 conv.
	#define PDM_SENS			( 1518500068.96869 )								// (2^23-1)*256 / ( sqrt(2) ) [raw/FS]
	#define ACCE_SENS			( 32767.0 / 9.81 )									// (2^15-1) [raw/FS] with g -> m/s2 conv.
	#define GYRO_SENS			( 32767.0 )

	#define ADC_Z_MULT			28.1
	#define ADC_X_MULT			27.9
	#define ACCE_MULT			( 1.0 / 16.0 ) 		// 16g == FS
	#define	GYRO_MULT			( 1.0 / 2000.0 )	// 2000dps == FS
	#define SENSOR_SENS			{ ( 1 ), ( 1 ), ( 1 ), ( 1 ), ( 1 ), ( 1 ), ( 1 ), ( 1 ), ( 1 ) } // sensors sensitivities [raw/SI]
	#define SENSOR_FRANGE		{ 0.5, 20000, 0.5, 20000, 10, 51200, 0, 1500, 0, 1500, 0, 1500, 0, 937, 0, 937, 0, 937 }	// [Hz]

#endif

/* ---------------------------------------------------------------------------------------- */

#define DEV_TCP_PORT			5000			// for setting device TCP server port
#define DEV_UDP_PORT			5000			// for setting device UDP port
#define DEV_UDP_CLIENT_PORT 	5001

#define DEF_SERIAL_NO			{ 192,168,1,100, DEV_TCP_PORT, DEVICE_SN }	// default serial number

#define MAX_ADC					1				// max number of real channels
#define MAX_NOCH				9				// max number of channels

#define MAX_POS					65536			// max number of samples per channel in single pack
#define DEF_POS					4096			// default number of samples per channel in single pack
#define MAX_NOS					( 65536 * 4 )	// max number of samples per channel in single measurement

#define CHANNEL_TYPE			{ {DEV_IN_AC, DEV_IN_AC, DEV_IN_AC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC} }
#define NOVCHT					1
#define DEF_MS					DEV_MASTER		// default card mode: master (0x01) slave (0x00)

#define	VALID_FS				{ FS_8192, FS_16384, FS_32768, FS_65536, FS_131072 }	// valid sample frequency [Hz]
#define NOVFS					5
#define MIN_REAL_FS				FS_8192

#define VALID_RANGE				{ {-R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25}, { R_1_25, R_1_25, R_1_25, R_1_25, R_1_25, R_1_25, R_1_25, R_1_25, R_1_25} }
#define NOVR					1
#define DEF_RANGE_MIN			{ -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25, -R_1_25 }
#define DEF_RANGE_MAX			{  R_1_25,  R_1_25,  R_1_25,  R_1_25,  R_1_25,  R_1_25,  R_1_25,  R_1_25,  R_1_25 }
#define DEF_RANGE_IDX			{  0,      0,      0,	   0,	   0,	   0,	   0,	   0,	   0 }

#define	DEF_FS					FS_8192			// default sample frequency [Hz]
#define DEF_NOS					8192			// default number of samples per channel
#define DEF_NOCH				MAX_NOCH		// default number of channels

#define FAB_CH_GAIN_INT			{ FAB_CH_GAIN_INTX }
#define FAB_CH_OFFSET_INT		{ { 0, 0, 0, 0, 0, 0, 0, 0, 0 } }
#define FAB_CH_GAIN_EXT			{ { 1, 1, 1, 1, 1, 1, 1, 1, 1 } }
#define FAB_CH_OFFSET_EXT		{ { 0, 0, 0, 0, 0, 0, 0, 0, 0 } }

#define DEF_SENSOR_SENS			SENSOR_SENS 													// fabric sensor sensitivity [mV/m/s2] !!!!!
#define DEF_UNIT				{ ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2, ADS_UNIT_ms2 }		// default unit [V] !!!!!!!
#define DEF_SENSOR_TYPE			{ DEV_IN_AC, DEV_IN_AC, DEV_IN_AC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC, DEV_IN_DC }

#define DEF_SENSOR_SW			SENSOR_INTERNAL


/** Wi-Fi Settings */

#define MAIN_WIFI_M2M_PRODUCT_NAME			"mVIBE"
#define MAIN_WLAN_AUTH						M2M_WIFI_SEC_WPA_PSK
#define MAIN_OTA_URL						"http://www.alitec.home.pl/pub/m2m_ota.bin"

#define WSOCK_0_IDX							( 0 )
#define WIFI_SOCKET_0_TYPE					SOCK_STREAM
#define WIFI_SOCKET_0_PORT					( DEV_TCP_PORT )

#define WSOCK_1_IDX							( 1 )
#define WIFI_SOCKET_1_TYPE					SOCK_STREAM
#define WIFI_SOCKET_1_PORT					( 5001 )

#define WSOCK_2_IDX							( 2 )
#define WIFI_SOCKET_2_TYPE					SOCK_DGRAM
#define WIFI_SOCKET_2_PORT					( DEV_UDP_PORT )

#define WSOCK_3_IDX							( 3 )
#define WIFI_SOCKET_3_TYPE					SOCK_STREAM
#define WIFI_SOCKET_3_PORT					( 5080 )

#define MAIN_WIFI_M2M_REPORT_INTERVAL		(1000)

#define MAIN_WIFI_M2M_BUFFER_SIZE			1460


#define BATTERY_CAPACITY					1100


#include "board_mvibe.h"
#include "protocol_v4.h"


#endif /* DEVICE_MVIBE_DB_H_ */
