/*
 * fm25vCL64.h
 *
 *  Created on: 12 gru 2017
 *      Author: KK
 */

#ifndef MAIN_FM25VCL64_H_
#define MAIN_FM25VCL64_H_

#include "esp_err.h"
#include "device_mvibe_db.h"
#include "mvibe_analog.h"

#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
    int cs_pin;
    int sck_pin;
    int mosi_pin;
    int miso_pin;
    int clk_freq_hz;
} spi_config_t;



esp_err_t fram_init();
esp_err_t fram_read_DevReg(device_register_t *DevReg, int length);
esp_err_t fram_write_DevReg(device_register_t *data, int length);
esp_err_t fram_write_SensorSens(sensor_sens_t *data, int length);
esp_err_t fram_read_SensorSens(sensor_sens_t *SensorSens, int length);


#ifdef __cplusplus
}
#endif

#endif  /* MAIN_FM25VCL64_H_ */
