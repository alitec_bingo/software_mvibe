#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_err.h>
#include "LSM6DSMTR.h"
#include "mvibe_analog.h"
#include "spi_master_mod.h"
#include "pcm186x.h"


static const char *TAG = "SPI";

#define FS_PIEZO 		( 8192.0 * 10 )
#define FS_MEMS			( 3285.0 * 10 )
#define MULT_PARAM		FS_MEMS / FS_PIEZO
#define NOPS			256
#define NOPS_FIRST		( NOPS - 1)

int prev_samples[ MAX_NOCH ];
int no_read_mems_samples[ MAX_NOCH ];
int psa[ MAX_NOCH ] = { NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST, NOPS_FIRST };
int p[ MAX_NOCH ] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int sample_k [ MAX_NOCH ] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int n [ MAX_NOCH ] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int8_t   chWraper[ MAX_NOCH ] = { 0, 0, 0, 3, 4, 5, 0, 1, 2 };


device_register_t				DevReg;
flags_wraper_led_t				Flags_Wraper;
spi_lobo_device_handle_t 		acce_bus;

static esp_err_t write_reg(const uint8_t data, const uint8_t data1, uint8_t length)
{
	spi_lobo_transaction_t t = {
        .flags = LB_SPI_TRANS_USE_RXDATA | LB_SPI_TRANS_USE_TXDATA,
        .length = length,
		.rxlength = 0,
        .tx_data[0] = data,
		.tx_data[1] = data1,

    };
    esp_err_t err = spi_lobo_transfer_data(acce_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE("write_reg", "Send buffer failed, err = %d", err);
    }

    return err;
}

esp_err_t acce_read_short(uint8_t addr, int16_t *data_out)
{
	spi_lobo_transaction_t t = {
        .flags = LB_SPI_TRANS_USE_RXDATA | LB_SPI_TRANS_USE_TXDATA,
        .length = 32,
        .tx_data[0] = addr | 0x80,
		.tx_data[1]=0,
		.tx_data[2]=0,
		.tx_data[3]=0,
        .rxlength  = 3 * 8,
    };

	esp_err_t err = spi_lobo_transfer_data(acce_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE("acce_read_short", "Send buffer failed, err = %d", err);
    }

    *data_out = *((int16_t*)(&t.rx_data[1]));

    return err;
}

esp_err_t acce_spi_init()
{

    esp_err_t err;
    spi_lobo_bus_config_t buscfg = {
    	.miso_io_num = 12,
        .mosi_io_num = 13,
        .sclk_io_num = 14,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 0,
    };

    spi_lobo_device_interface_config_t devcfg = {
        .clock_speed_hz = 10000000,
        .mode = 3,
        .spics_io_num = 27,
		.flags = 0,
    };

    err = spi_lobo_bus_add_device(TFT_HSPI_HOST, &buscfg, &devcfg, &acce_bus );
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "SPI bus device add failed, err = %d", err);
    }

    return err;
}

void acce_set_reg()
{
	spi_lobo_bus_remove_device(acce_bus);
	acce_spi_init();
	write_reg(0x12, 0x45, 16);		  // Reset
	vTaskDelay(50);
	write_reg(0x12, 0xC4, 16);        // Reboot memory, 1- output registers not updated until MSB and LSB have been read, 1- interrupt output pads active low
	vTaskDelay(50);
	write_reg(0x12, 0x44, 16);
	write_reg(0x06, 0x70, 16);        // FIFO_CTRL1: FIFO WATERMARK: 256 Sa x 6ch = 1536 = 0x600
	write_reg(0x07, 0x02, 16);        // FIFO_CTRL2: FIFO WATERMARK, STEP COUNTER, DATA READY, TEMP NO INCLUDED IN FIFO
	write_reg(0x08, 0x09, 16);        // FIFO_CTRL3: GYRO ACCE No decimation
	write_reg(0x09, 0x00, 16);        // FIFO_CTRL4: THIRD FOURTH data not in fifo, fifo depth no limited, only high data disable
	write_reg(0x0A, 0x56, 16);        // FIFO_CTRL5: FIFO ODR is set to 6.66 kHz, FIFO mode.
									  // Continuous mode. If the FIFO is full, the new sample overwrites the older one.
	write_reg(0x0d, 0x08, 16);        // INT1_CTRL: FIFO threshold interrupt on INT1 pad enabled
	write_reg(0x0e, 0x00, 16);        // INT2_CTRL: disable INT2
	write_reg(0x10, 0xA4, 16);        // CTRL1_XL: 6.66 kHz (high performance), �16 g, BW @ 1.5 kHz, LPF off
	write_reg(0x11, 0xAC, 16);        // CTRL2_G: GYRO output data 6.66 kHz (high performance), Gyroscope full-scale 2000dps, Gyroscope full-scale at 125 dps disable
	write_reg(0x13, 0x84, 16);        // CTRL4_C: Extend DEN functionality to accelerometer sensor disable, SPI only
	write_reg(0x14, 0x60, 16);        // !!!  CTRL5_C: DEN active level configuration active high
	write_reg(0x15, 0x83, 16);        // CTRL6_C: DEN data edge-sensitive trigger enable - INT2 pin is switched to input mode (DEN signal)
	write_reg(0x16, 0x04, 16);        // !!!  CTRL7_G: default
	write_reg(0x17, 0x00, 16);        // CTRL8_XL: low-pass filter LPF2 selection and high-pass
	write_reg(0x18, 0x00, 16);        // CTRL9_XL: DEN-X Y Z not stored in LSB
	write_reg(0x19, 0x00, 16);        // CTRL10_C: WRIST_TILT, PEDO, SIGN_MOTION, TILT, TIMER disabled

	ESP_LOGI(TAG, "\n write reg \n");
}

void acce_set_reg_flush()
{

	if(!Flags_Wraper.no_mems_flag)
	{
		write_reg(0x12, 0x45, 16);		  // Reset
		vTaskDelay(50);
		write_reg(0x12, 0xC4, 16);        // Reboot memory, 1- output registers not updated until MSB and LSB have been read, 1- interrupt output pads active low
		vTaskDelay(50);
		write_reg(0x12, 0x44, 16);
		write_reg(0x06, 0x70, 16);        // FIFO_CTRL1: FIFO WATERMARK: 256 Sa x 6ch = 1536 = 0x600
		write_reg(0x07, 0x02, 16);        // FIFO_CTRL2: FIFO WATERMARK, STEP COUNTER, DATA READY, TEMP NO INCLUDED IN FIFO
		write_reg(0x08, 0x09, 16);        // FIFO_CTRL3: GYRO ACCE No decimation
		write_reg(0x09, 0x00, 16);        // FIFO_CTRL4: THIRD FOURTH data not in fifo, fifo depth no limited, only high data disable
		write_reg(0x0A, 0x56, 16);        // FIFO_CTRL5: FIFO ODR is set to 6.66 kHz, FIFO mode.
										  // Continuous mode. If the FIFO is full, the new sample overwrites the older one.
		write_reg(0x0d, 0x08, 16);        // INT1_CTRL: FIFO threshold interrupt on INT1 pad enabled
		write_reg(0x0e, 0x00, 16);        // INT2_CTRL: disable INT2
		write_reg(0x10, 0xA4, 16);        // CTRL1_XL: 6.66 kHz (high performance), �16 g, BW @ 1.5 kHz, LPF off
		write_reg(0x11, 0xAC, 16);        // CTRL2_G: GYRO output data 6.66 kHz (high performance), Gyroscope full-scale 2000dps, Gyroscope full-scale at 125 dps disable
		write_reg(0x13, 0x84, 16);        // CTRL4_C: Extend DEN functionality to accelerometer sensor disable, SPI only
		write_reg(0x14, 0x60, 16);        // !!!  CTRL5_C: DEN active level configuration active high
		write_reg(0x15, 0x83, 16);        // CTRL6_C: DEN data edge-sensitive trigger enable - INT2 pin is switched to input mode (DEN signal)
		write_reg(0x16, 0x04, 16);        // !!!  CTRL7_G: default
		write_reg(0x17, 0x00, 16);        // CTRL8_XL: low-pass filter LPF2 selection and high-pass
		write_reg(0x18, 0x00, 16);        // CTRL9_XL: DEN-X Y Z not stored in LSB
		write_reg(0x19, 0x00, 16);        // CTRL10_C: WRIST_TILT, PEDO, SIGN_MOTION, TILT, TIMER disabled
	}

	vTaskDelay(10);
	pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, PCM186X_REG_RESET );		//Resetowanie PCM
	pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, 0x03 );						// select Page3
	pcm_wirte_reg( PCM186X_REG_GPIO_1_0_DIR, 0x40 );
	pcm_wirte_reg( PCM186X_REG_MIC_BIAS_CTRL, 0x00 );					// 0x15
	pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, 0x00 );						// select Page0
	pcm_wirte_reg( PCM186X_REG_PWDOWN_CONF, 0b01110001 );				// 0x70 - standby
	pcm_wirte_reg( PCM186X_REG_ADC1L_IN_SEL, 0b01000010 );				// 0x06 - VIN1L2
	pcm_wirte_reg( PCM186X_REG_ADC1R_IN_SEL, 0b01000010 );				// 0x07 - VIN1R2
	pcm_wirte_reg( PCM186X_REG_ADC2L_IN_SEL, 0b01000000 );				// 0x08 - none
	pcm_wirte_reg( PCM186X_REG_ADC2R_IN_SEL, 0b01000000 );				// 0x09 - none
	pcm_wirte_reg( PCM186X_REG_GPIO_1_0_FUNC, 0b00010001 );				// 0x10 - GPIO0 and GPIO1 as DIGMIC_IN0 and DIGMIC_IN1
	pcm_wirte_reg( PCM186X_REG_GPIO_3_2_FUNC, 0b00000001 );				// 0x11 - GPIO2 as DIGMIC_CLK
	pcm_wirte_reg( PCM186X_REG_GPIO_PULLDOWN, 0b00000000 );				// 0x15
	pcm_wirte_reg( PCM186X_REG_DPGA_CH_2_1_CTRL, 0xff );				// 0x19 - DPGA manual config (set 0dB)
	pcm_wirte_reg( PCM186X_REG_AUDIO_INTERFACE_FORMAT, 0b01000100 );	// 0x0B
	pcm_wirte_reg( PCM186X_REG_TDM_OSEL, 0b00000000 );					// 0x0C
	pcm_wirte_reg( PCM186X_REG_TX_TDM_OFFSET, 0b00000001 );				// 0x0D
	pcm_wirte_reg( PCM186X_REG_RX_TDM_OFFSET, 0b00000001 );				// 0x0E
	pcm_wirte_reg( PCM186X_REG_DIN_RESAMP, 0b00000001 );				// 0x1B
	pcm_wirte_reg( PCM186X_REG_CLOCK_CONF, 0b00101110 );				// 0x20
	pcm_wirte_reg( PCM186X_REG_PLL_FRAC_JD_DIV_LSB, 0 );				// 0x2C
	pcm_wirte_reg( PCM186X_REG_PLL_FRAC_JD_DIV_MSB, 0 );				// 0x2D
	pcm_wirte_reg( PCM186X_REG_INT_CONF1, 0x00 );						// 0x60
	pcm_wirte_reg( PCM186X_REG_DSP_CTRL, 0b10011100 );					// 0x71
	pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 31 );			//21
	pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 31 );			//22
	pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  63 );			//23
	pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	31 );		// 0x25
	pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
	pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27
	pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 0 );					//29
	pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 3 );					//2A
	pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			//2B
	pcm_wirte_reg( PCM186X_REG_PLL_CONF, 0b00000011 );					// 0x28 - start PLL
	pcm_wirte_reg( PCM186X_REG_PWDOWN_CONF, 0b01110000 );				// 0x70 - run

	ESP_LOGI(TAG, "\n FLUSH DONE  \n");
}


void acce_flush_fifo( void )
{
//	int16_t 	buf_fifo;
	pcm_wirte_reg( PCM186X_REG_PWDOWN_CONF, 0x77 );			//Power down

	if(!Flags_Wraper.no_mems_flag)
	{
//		for( int i = 0; i < 4096; i++ )
//		{
//			acce_read_short( 0x3E, &buf_fifo );
//		}
		write_reg( 0x0A, 0x50, 16 );			//BYPAS MODE
	}

	acce_set_reg_flush();
}

static int number_of_read_samples(int number_of_mems_ch)
{
	int read_mems_samples;
	float full_no_mems_samples;

	if( psa[ number_of_mems_ch ] > ( FS_PIEZO - 1 ))
		psa[ number_of_mems_ch ] = NOPS_FIRST;

	if(psa[ number_of_mems_ch ] == NOPS_FIRST )
	{
		read_mems_samples = MULT_PARAM * psa[ number_of_mems_ch ];
		prev_samples[ number_of_mems_ch ] = read_mems_samples;
		psa[ number_of_mems_ch ] += NOPS;

		if( Flags_Wraper.flag )
		{
			return read_mems_samples + 1;
		}
		else
			return read_mems_samples ;
	}

	full_no_mems_samples = MULT_PARAM * psa[ number_of_mems_ch ];
	psa[ number_of_mems_ch ] += NOPS;
	read_mems_samples = (int)full_no_mems_samples - prev_samples[ number_of_mems_ch ];
	prev_samples[ number_of_mems_ch ] += read_mems_samples;

	return read_mems_samples;
}

static void interpolation_filter(int32_t *samples, int read_mems_samples, int number_of_mems_ch  )
{
	float k;
	float k_ulamk;
	int k_calk;
	int k_prim_calk;
	int32_t *wsk = samples;
	int32_t samples_start[ read_mems_samples + 2 ];
	static int32_t sample_0[ 9 ];
	static int32_t sample_1[ 9 ];

		if( !Flags_Wraper.flag )
		{
			samples_start[ 0 ] = sample_0[ number_of_mems_ch ];
			samples_start[ 1 ] = sample_1[ number_of_mems_ch ];

			for(int i = 2; i < read_mems_samples + 2; i++)
			{
				samples_start[i] = *samples ++;
			}
		}
		else
		{
			for( int i = 0; i < read_mems_samples; i++ )
				{
					samples_start[i] = *samples ++;
				}
		}
		for(int i = 0; i < NOPS; i++)
		{
			k = n[ number_of_mems_ch ] * MULT_PARAM;
			k_calk = (int)k;
			k_ulamk = k - k_calk;
			k_prim_calk =  k_calk - sample_k[ number_of_mems_ch ];
			*wsk++ = samples_start[ k_prim_calk ] + (float)( samples_start[ k_prim_calk + 1 ] - samples_start[ k_prim_calk ]) * k_ulamk;
			n[ number_of_mems_ch ]++;
		}

		if( !Flags_Wraper.flag )
		{
			sample_0[ number_of_mems_ch ] = samples_start[ read_mems_samples ];
			sample_1[ number_of_mems_ch ] = samples_start[ read_mems_samples + 1 ];
		}
		else
		{
			sample_0[ number_of_mems_ch ] = samples_start[ read_mems_samples -1];
			sample_1[ number_of_mems_ch ] = samples_start[ read_mems_samples ];

		}

		if( prev_samples[ number_of_mems_ch ] < FS_MEMS - 1 )
			sample_k[ number_of_mems_ch ] = prev_samples[ number_of_mems_ch ];
		else
			sample_k[ number_of_mems_ch ] = 0;

		if( n[ number_of_mems_ch ] == FS_PIEZO )
			n[ number_of_mems_ch ] = 0;
}
esp_err_t multichannel_read_samples( int32_t * buf, uint32_t * len)
{
		int32_t * buf_out = buf;
		esp_err_t err;
		uint16_t rcv_pattern;
		uint16_t data_read;
		uint16_t rcv_watermark = 0;
		int nos = 0;
		uint32_t 	idx;
		int16_t 	buf_fifo[ LSM_FIFO_WM_SIZE ];
		int16_t 	* buf_fifo_ptr = buf_fifo;
		int32_t 	temp_buf_fifo[ LSM_FIFO_WM_SIZE ];

		if(Flags_Wraper.flag)
		{

			if( Flags_Wraper.no_mems_flag == 0 )
			{
				acce_flush_fifo();
			}
			for( int i = 0; i < 4 * I2S_DMA_COUNT; i++) // clean i2s DMA buffer
				{
					i2s_mesaurement( (char*)buffer_i2s_send, I2S_BUFFER_SIZE * sizeof(uint32_t) );
				}
		}

		i2s_mesaurement( (char*)buffer_i2s, I2S_BUFFER_SIZE * sizeof(uint32_t) );

		if( DevReg.SampleFrequency == 8192 && !Flags_Wraper.no_mems_flag )
		{
			err = acce_read_short(0x3A, (int16_t*)&data_read);
				if (err != ESP_OK)
					ESP_LOGE(TAG, "Read error = %d", err);

			rcv_watermark = data_read & 0x8000;

			while( !rcv_watermark)
			{
				err = acce_read_short(0x3A, (int16_t*)&data_read);
					if (err != ESP_OK)
						ESP_LOGE(TAG, "Read error = %d", err);

				rcv_watermark = data_read & 0x8000;
			}

			if( rcv_watermark )
			{
				err = acce_read_short(0x3C, (int16_t*)&rcv_pattern);
					if (err != ESP_OK)
						ESP_LOGE(TAG, "Read error = %d", err);

				if ( rcv_pattern >= NOCH_LSM )
				{
					*len = 0;
					return err;
				}
				for( int m = 3; m < MAX_NOCH; m++ )
				{
					switch( m )
					{
						case 3:
							chWraper[ m ] = 3 - rcv_pattern;		// acc x
							break;
						case 4:
							chWraper[ m ] = 4 - rcv_pattern;		// acc y
							break;
						case 5:
							chWraper[ m ] = 5 - rcv_pattern;		// acc z
							break;
						case 6:
							chWraper[ m ] = 0 - rcv_pattern;		// gyro x
							break;
						case 7:
							chWraper[ m ] = 1 - rcv_pattern;		// gyro y
							break;
						case 8:
							chWraper[ m ] = 2 - rcv_pattern;		// gyro z
							break;
						default:
							chWraper[ m ] = 0;
							break;
					}
					if ( chWraper[ m ] < 0 )
					{
						chWraper[ m ] += NOCH_LSM;
					}
				}
				for( int i = 0; i < DevReg.NumberOfChannels; i++ )
				{
					if( DevReg.ChSequence[ i ] >= 3 )
						nos = number_of_read_samples( DevReg.ChSequence[ i ] );
				}
				for( int i = 0; i < NOCH_LSM * nos ; i++ )
				{
					err = acce_read_short( 0x3E, &buf_fifo[ i ] );
					if ( err != ESP_OK )
						ESP_LOGE(TAG, "Read error = %d", err);
				}
			}
			else
			{
				*len = 0;
				return err;
			}
		}

	for( int i = 0; i < DevReg.NumberOfChannels; i++ )
	{
		buf_fifo_ptr = buf_fifo;
		buf_out = buf + i;
		uint32_t *buffer_piezo = buffer_i2s;

			if( DevReg.ChSequence[ i ]  == 0 || DevReg.ChSequence[ i ] == 2 )
			{
				for(int j = 0; j < 256; j++)
				{
					*buf_out = *buffer_piezo;
					buffer_piezo += 2;
					buf_out += DevReg.NumberOfChannels;
				}
			}
			else if( DevReg.ChSequence[ i ] == 1 )
			{
				buffer_piezo++;
				for( int j = 0; j < 256; j++ )
				{
					*buf_out = *buffer_piezo;
					buffer_piezo += 2;
					buf_out += DevReg.NumberOfChannels;
				}
			}
			else
			{
				idx = chWraper[ DevReg.ChSequence[ i ] ];
				for( int j = 0; j < nos ; j++ )
					{
						temp_buf_fifo[ j ] = buf_fifo_ptr[ idx ];
						buf_fifo_ptr += NOCH_LSM;
					}

				interpolation_filter( &temp_buf_fifo[ 0 ] , nos, DevReg.ChSequence[ i ] );

				for( int d = 0; d < 256; d++ )
					{
						*buf_out = temp_buf_fifo[ d ];
						buf_out += DevReg.NumberOfChannels;
					}
			}
	}

	if( Flags_Wraper.flag == 1 )
		Flags_Wraper.flag = 0;

	*len = NOSA_LSM * DevReg.NumberOfChannels;
	return ESP_OK;
}

