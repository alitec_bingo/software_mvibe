/*
 * fm25vCL64.c
 *
 *  Created on: 12 gru 2017
 *      Author: KK
 */


#include "fm25vCL64.h"
#include "string.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "esp_log.h"
#include "driver/rtc_io.h"
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include "esp_err.h"

static const char *TAG = "SPI";

spi_device_handle_t fram_bus;


// W tych dw�ch funkcjach u�ywana jest tablica zamiast wska�nika do wysy�ania i zapisywania
// DZIA�A !

static esp_err_t fram_write(const uint8_t data, const uint8_t data1, const uint8_t data2, const uint8_t data3, uint8_t length)
{
	spi_transaction_t t = {
        .flags = SPI_TRANS_USE_RXDATA | SPI_TRANS_USE_TXDATA,
        .length = length,
        .tx_data[0] = data,
		.tx_data[1] = data1,
		.tx_data[2] = data2,
		.tx_data[3] = data3

    };
    esp_err_t err = spi_device_transmit(fram_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Send buffer failed, err = %d", err);
    }
    return err;
}

static esp_err_t fram_write_pointer(device_register_t *data, int length)
{

	spi_transaction_t t = {
        .flags = 0,
        .length =( 24 + (length *8) ) ,
		.addr = 0x020000,
		.tx_buffer = data,

    };
    esp_err_t err = spi_device_transmit(fram_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Send buffer failed, err = %d", err);
    }

    return err;
}

static esp_err_t fram_read_pointer(device_register_t *data, int length)
{

	spi_transaction_t t = {
        .flags = 0,
        .length = (length * 8 + 24),
        .rxlength  = length * 8 ,
		.rx_buffer = data,
		.addr = 0x030000
    };
    esp_err_t err = spi_device_transmit(fram_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Send buffer failed, err = %d", err);
    }

    return err;

}

esp_err_t fram_spi_init()
{
    esp_err_t err;
    spi_bus_config_t buscfg = {
    	.miso_io_num = 12,
        .mosi_io_num = 13,
        .sclk_io_num = 14,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 0,
    };

    spi_device_interface_config_t devcfg = {
        .clock_speed_hz = 20000000,
        .mode = 0,
        .spics_io_num = 15,
        .queue_size = 1,
		.flags = 0,
    };
    err = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "SPI bus initialization failed, err = %d", err);
    }
    err = spi_bus_add_device(HSPI_HOST, &devcfg, &fram_bus);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "SPI bus device add failed, err = %d", err);
    }
    return err;
}



esp_err_t fram_spi_init_pointer()
{
    esp_err_t err;
    spi_bus_config_t buscfg = {
    	.miso_io_num = 12,
        .mosi_io_num = 13,
        .sclk_io_num = 14,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 0,

    };

    spi_device_interface_config_t devcfg = {
        .clock_speed_hz = 20000000,
        .mode = 0,
        .spics_io_num = 15,
        .queue_size = 1,
		.flags = 0,
		.address_bits = 3*8,
    };
    err = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "SPI bus initialization failed, err = %d", err);
    }
    err = spi_bus_add_device(HSPI_HOST, &devcfg, &fram_bus);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "SPI bus device add failed, err = %d", err);
    }
    return err;
}

esp_err_t fram_init()
{
	esp_err_t err;

	fram_spi_init();

	err  = fram_write(0x06, 0 ,0, 0, 8);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	err  = fram_write(0x01, 0x80, 0, 0, 16);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	return err;
}


esp_err_t fram_read_DevReg(device_register_t *DevReg, int length)
{
	esp_err_t err;

	fram_spi_init();

	err  = fram_write(0x06, 0 ,0, 0, 8);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	fram_spi_init_pointer();

	err  = fram_read_pointer(DevReg, length);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "read error, err = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	return err;
}

esp_err_t fram_write_DevReg(device_register_t *data, int length)
{
	esp_err_t err;

	fram_spi_init();

	err  = fram_write(0x06, 0 ,0, 0, 8);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	fram_spi_init_pointer();
	fram_write_pointer(data, length);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	return err;
}

static esp_err_t fram_write_pointer_sens(sensor_sens_t *data, int length)
{

	spi_transaction_t t = {
        .flags = 0,
        .length =( 24 + (length *8) ) ,
		.addr = 0x0201FF,
		.tx_buffer = data,

    };
    esp_err_t err = spi_device_transmit(fram_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Send buffer failed, err = %d", err);
    }

    return err;
}

static esp_err_t fram_read_pointer_sens(sensor_sens_t *data, int length)
{

	spi_transaction_t t = {
        .flags = 0,
        .length = (length * 8 + 24),
        .rxlength  = length * 8 ,
		.rx_buffer = data,
		.addr = 0x0301FF,
    };
    esp_err_t err = spi_device_transmit(fram_bus, &t);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Send buffer failed, err = %d", err);
    }

    return err;

}

esp_err_t fram_write_SensorSens(sensor_sens_t *data, int length)
{
	esp_err_t err;

	fram_spi_init();

	err  = fram_write(0x06, 0 ,0, 0, 8);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	fram_spi_init_pointer();
	fram_write_pointer_sens(data, length);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	return err;
}

esp_err_t fram_read_SensorSens(sensor_sens_t *SensorSens, int length)
{
	esp_err_t err;

	fram_spi_init();

	err  = fram_write(0x06, 0 ,0, 0, 8);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "Write error = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	fram_spi_init_pointer();

	err  = fram_read_pointer_sens(SensorSens, length);
	if (err != ESP_OK)
		ESP_LOGE(TAG, "read error, err = %d", err);

	spi_bus_remove_device(fram_bus);
	spi_bus_free(HSPI_HOST);

	return err;
}
