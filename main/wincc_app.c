/*
 * winc_app.c
 *
 * Created: 2015-04-10 09:27:38
 *  Author: mmakow
 */

#include <string.h>



#include "device_mvibe_db.h"
#include "wincc_app.h"

/* ---------------------------------------------------------------------------------------- */

/** Socket number/status for TCP/UDP communication */

my_sockets		mySockReg[ MAX_SOCKET ];

/* ---------------------------------------------------------------------------------------- */


uint8_t get_listen_socket_struct_idx( SOCKET sock )
{
	uint8_t idx;

	for ( idx = 0; idx < MAX_SOCKET; idx++ )
	{
		if ( mySockReg[ idx ].listenSocketHdl == sock )
			break;
		else
			continue;
	}
	return idx;
}


void reset_struct_mySockReg( int mySockIdx )
{
	memset(&mySockReg[ mySockIdx ].socketAddr, 0, sizeof(struct sockaddr_in));

	mySockReg[ mySockIdx ].listenSocketHdl =			-1;
	mySockReg[ mySockIdx ].acceptedSocketHdl =			-1;
	mySockReg[ mySockIdx ].socketAddr.sin_family =		AF_INET;
	mySockReg[ mySockIdx ].socketAddr.sin_port =		htons( 5000 );
	mySockReg[ mySockIdx ].socketAddr.sin_addr.s_addr =	htonl(INADDR_ANY);
	mySockReg[ mySockIdx ].recvLen =					0;
	mySockReg[ mySockIdx ].FlagCheckSocketTcp = 		false;
}


void struct_mySockReg_init( void )
{
	/* Initialize socket address structure. */

	memset(&mySockReg[ WSOCK_0_IDX ].socketAddr, 0, sizeof(struct sockaddr_in));

	mySockReg[ WSOCK_0_IDX ].listenSocketHdl =				-1;
	mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl =			-1;
	mySockReg[ WSOCK_0_IDX ].socketAddr.sin_family =		AF_INET;
	mySockReg[ WSOCK_0_IDX ].socketAddr.sin_port =			htons( 5000 );
	mySockReg[ WSOCK_0_IDX ].socketAddr.sin_addr.s_addr =	htonl(INADDR_ANY);
	mySockReg[ WSOCK_0_IDX ].recvLen =						0;
	mySockReg[ WSOCK_0_IDX ].FlagCheckSocketTcp =			false;

	memset(&mySockReg[ WSOCK_2_IDX ].socketAddr, 0, sizeof(struct sockaddr_in));

	mySockReg[ WSOCK_2_IDX ].listenSocketHdl =				-1;
	mySockReg[ WSOCK_2_IDX ].acceptedSocketHdl =			-1;
	mySockReg[ WSOCK_2_IDX ].socketAddr.sin_family =		AF_INET;
	mySockReg[ WSOCK_2_IDX ].socketAddr.sin_port =			htons( 5000 );
	mySockReg[ WSOCK_2_IDX ].socketAddr.sin_addr.s_addr =	htonl(IPADDR_ANY);
	mySockReg[ WSOCK_2_IDX ].recvLen =						0;
	mySockReg[ WSOCK_2_IDX ].FlagCheckSocketTcp =			false;
}


void int_to_tab_ip( uint32_t ip, uint8_t *tab_ip )
{
	tab_ip[0] = ip & 0xFF;
	tab_ip[1] = (ip >> 8) & 0xFF;
	tab_ip[2] = (ip >> 16) & 0xFF;
	tab_ip[3] = (ip >> 24) & 0xFF;
}
