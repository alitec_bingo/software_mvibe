/*
 * MAX17055.h
 *
 *  Created on: 6 gru 2017
 *      Author: Koperek
 */

#ifndef MAIN_MAX17055_H_
#define MAIN_MAX17055_H_

#include "device_mvibe_db.h"
#include "mvibe_analog.h"


typedef struct
{
	int16_t		BatteryVoltage;		// voltage of battery [mV]
	int16_t		RARC;				// battery state of charge [%]
	int16_t		RAAC;				// battery state of charge [mAh]
	int16_t		BatteryCurrent; 	// battery current[mA]
	int16_t		BatteryTemperature; // tempetature of battery [*C]
	int16_t		FullCapacity;		// full capacity of the battery;

} fuel_gaguge_t;

fuel_gaguge_t FuelReg;

void max17055_init(device_register_t DevReg);
int max17055_read(fuel_gaguge_t *FuelReg );

#endif /* MAIN_MAX17055_H_ */
