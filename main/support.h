/*
 * support.h
 *
 *  Created on: 5 pa� 2017
 *      Author: admin
 */

#ifndef MAIN_SUPPORT_H_
#define MAIN_SUPPORT_H_


typedef enum{
	SOCKET_MSG_BIND	= 1,
	/*!<
		Bind socket event.
	*/
	SOCKET_MSG_LISTEN,
	/*!<
		Listen socket event.
	*/
	SOCKET_MSG_DNS_RESOLVE,
	/*!<
		DNS Resolution event.
	*/
	SOCKET_MSG_ACCEPT,
	/*!<
		Accept socket event.
	*/
	SOCKET_MSG_CONNECT,
	/*!<
		Connect socket event.
	*/
	SOCKET_MSG_RECV,
	/*!<
		Receive socket event.
	*/
	SOCKET_MSG_SEND,
	/*!<
		Send socket event.
	*/
	SOCKET_MSG_SENDTO,
	/*!<
		sendto socket event.
	*/
	SOCKET_MSG_RECVFROM
	/*!<
		Recvfrom socket event.
	*/
}tenuSocketCallbackMsgType;

#endif /* MAIN_SUPPORT_H_ */
