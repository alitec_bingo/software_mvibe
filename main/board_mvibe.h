/*
 * board_mvibe.h
 *
 *  Created on: 19 sty 2018
 *      Author: mmakow
 */

#ifndef MAIN_BOARD_MVIBE_H_
#define MAIN_BOARD_MVIBE_H_


/* ---------------------------------------------------------------------------------------- */

#define delay_ms(ms) 	( vTaskDelay( (ms) / portTICK_RATE_MS ) )

/* ---------------------------------------------------------------------------------------- */

#define WS2812_PIN						17

/* ---------------------------------------------------------------------------------------- */

#define SDA_PIN							GPIO_NUM_26
#define SCL_PIN							GPIO_NUM_25

#define PCM_I2C_ADDRESS 				0x4A
#define FG_I2C_ADDRESS 					0x36

#define I2C_EXAMPLE_MASTER_FREQ_HZ    	400000     			/*!< I2C master clock frequency */
#define WRITE_BIT  						I2C_MASTER_WRITE 	/*!< I2C master write */
#define READ_BIT   						I2C_MASTER_READ  	/*!< I2C master read */
#define ACK_CHECK_EN   					0x1     			/*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS  					0x0     			/*!< I2C master will not check ack from slave */
#define ACK_VAL    						0x0     	   	 	/*!< I2C ack value */
#define NACK_VAL   						0x1 	        	/*!< I2C nack value */

#endif /* MAIN_BOARD_MVIBE_H_ */
