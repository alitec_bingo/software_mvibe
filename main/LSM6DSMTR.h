#ifndef MAIN_LSM6DSMTR_H_
#define MAIN_LSM6DSMTR_H_

#include "esp_err.h"
#include "device_mvibe_db.h"
#include "mvibe_analog.h"


#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    int cs_pin;
    int sck_pin;
    int mosi_pin;
    int miso_pin;
    int clk_freq_hz;
} spi_acce_config_t;

void acce_set_reg();
esp_err_t acce_spi_init();
esp_err_t multichannel_read_samples( int32_t * buf, uint32_t * len);

#ifdef __cplusplus
}
#endif




#endif /* MAIN_LSM6DSMTR_H_ */
