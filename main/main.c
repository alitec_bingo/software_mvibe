#include "sdkconfig.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_system.h>
#include <esp_sleep.h>
#include <esp_log.h>
#include <esp_err.h>
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <nvs.h>
#include <nvs_flash.h>
#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <driver/touch_pad.h>
#include <driver/spi_master.h>
#include <sys/socket.h>
#include <esp_task_wdt.h>
#include "device_mvibe_db.h"
#include "main.h"
#include "wincc_app.h"
#include "mvibe_analog.h"
#include "network_update.h"
#include "update.h"
#include "pcm186x.h"
#include "max17055.h"
#include "ws2812B.h"
#include "fm25vCL64.h"
#include "LSM6DSMTR.h"

char 							tag[] = "MAIN";

static volatile uint8_t			ErrCnt;																// Error Counter
static volatile uint8_t 		ErrBuffer[ MAX_ERR_CNT + 1 ] __attribute__ ((aligned (4)));			// Error Buffer

/* ---------------------------------------------------------------------------------------- */

device_register_t				DevReg;
device_state_t					DevState;
device_acquire_t				DevAcquire;
sensor_sens_t					SensorSens;
flags_wraper_led_t				Flags_Wraper;

/* ---------------------------------------------------------------------------------------- */

EventGroupHandle_t 				wifi_sta_event_group;
bool 							flag_check_status_ap = false;										// Checking connection status
uint8_t							gau8SocketBufferUDP[ UDP_BUFF_SIZE ] __attribute__((aligned (4)));
uint8_t							gau8SocketBuffer[ TCPIP_BUFF_SIZE ] __attribute__((aligned (4)));
uint8_t							recvCmdBuff[ MAX_TCP_BUFF_SIZE ] __attribute__ ((aligned (4)));		// TCP/IP RX buffer in SRAM
uint8_t							ucTxBuf[ 1024 ] __attribute__ ((aligned (4)));
message_udp_t					msgUDP;
uint8_t data;

bool measure_flag = 0;

/* ---------------------------------------------------------------------------------------- */

card_param_t card_param =
{
	.DefSerialNo = 				DEF_SERIAL_NO,
	.DefCardType = 				CARD_TYPE,
	.MaxNumberOfSamples = 		MAX_NOS,
	.MaxNumberOfChannels = 		MAX_NOCH,
	.NoVChT = 					NOVCHT,
	.DefChannelType = 			CHANNEL_TYPE,
	.NoVFS = 					NOVFS,
	.DefValidFS = 				VALID_FS,
	.NoVR = 					NOVR,
	.DefValidRange = 			VALID_RANGE,
	.NoSensor = 				NOSENSORS,
	.SensorIntExtSw = 			DEF_SENSOR_SW,
	.DefSensorSens = 			SENSOR_SENS,
	.DefSensorFreqRange = 		SENSOR_FRANGE,
	.FabChGainInt = 			FAB_CH_GAIN_INT,
	.FabChOffsetInt = 			FAB_CH_OFFSET_INT,
	.FabChGainExt = 			FAB_CH_GAIN_EXT,
	.FabChOffsetExt = 			FAB_CH_OFFSET_EXT,
};

/* ---------------------------------------------------------------------------------------- */

extern my_sockets				mySockReg[ MAX_SOCKET ];

/* ---------------------------------------------------------------------------------------- */

static void dev_reg_init( void );
static void deep_sleep_gpio_set( void );
static int send_tcp( SOCKET sock, uint8_t *buf, uint32_t len );
static void send_error_buffer( int sockIdx );

/* ---------------------------------------------------------------------------------------- */

// flag: 0 - do nothing; 1 - freeze color; 2 - unfreeze color
void led( uint8_t r, uint8_t g, uint8_t b, uint8_t flag )
{
	static uint8_t frezeFlag = 0;
	static rgbVal oldColor;

	rgbVal newColor;

	if ( flag == 1 )
	{
		frezeFlag = 1;
		newColor = makeRGBVal( r >> 2, g >> 2, b >> 2 );
		ws2812_setColors( 1, &newColor );
		return;
	}
	else if ( flag == 2 )
	{
		frezeFlag = 0;
		ws2812_setColors( 1, &oldColor );
		return;
	}

	if ( frezeFlag == 1 )
		return;

	newColor = makeRGBVal( r >> 2, g >> 2, b >> 2 );
	ws2812_setColors( 1, &newColor );
	oldColor = newColor;
}
//
static esp_err_t event_handler_sta( void *ctx, system_event_t *event )
{
	#define retry_count 	20
	static int retry 		= 0;
	char tag[] = "event_handler";

	switch( event->event_id )
	{
    	case SYSTEM_EVENT_STA_START:
    		ESP_LOGI( tag, "SYSTEM_EVENT_STA_START!" );
    		ESP_ERROR_CHECK(tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, "Alitec_mVIBE"));//WIFI_NAME
    		esp_wifi_connect();
    		break;

    	case SYSTEM_EVENT_STA_DISCONNECTED:
    		ESP_LOGI( tag, "SYSTEM_EVENT_STA_DISCONNECTED!" );
    		esp_wifi_connect();
    		xEventGroupClearBits( wifi_sta_event_group, WIFI_CONNECTED_BIT );

    		if(led_flag == 1){ led( 255,0,0, 0 ); }												// ustawienie diody na czerwony
			if ( ++retry <= retry_count )
			{
			   ESP_LOGI( tag, "Waiting for system time to be set... (%d/%d)", retry, retry_count );
			   vTaskDelay( 1000 / portTICK_PERIOD_MS );

			   if( retry == retry_count )
			   {
					retry = 0;
					vTaskDelay( 10 );
					deep_sleep_gpio_set();
			   }
			}
			break;

    	case SYSTEM_EVENT_STA_CONNECTED:
    		ESP_LOGI( tag, "SYSTEM_EVENT_STA_CONNECTED!" );

    		if(led_flag == 1){ led( 0,0,255, 0 ); }												// ustawienie diody na niebieski
			retry = 0;
			break;

    	case SYSTEM_EVENT_STA_GOT_IP:
    		ESP_LOGI( tag, "SYSTEM_EVENT_STA_GOT_IP!" );
    		xEventGroupSetBits( wifi_sta_event_group, WIFI_CONNECTED_BIT );

    		uint8_t tabIp[ 4 ];
			int_to_tab_ip( event->event_info.got_ip.ip_info.ip.addr, tabIp );
			for( int i=0; i<4; i++ ) { DevReg.SerialNumber_WiFi[ i ] = tabIp[ i ]; }
			break;

    	case SYSTEM_EVENT_AP_STACONNECTED:
			ESP_LOGI( tag, "station:"MACSTR" join,AID=%d", MAC2STR(event->event_info.sta_connected.mac), event->event_info.sta_connected.aid);
			xEventGroupSetBits( wifi_sta_event_group, WIFI_CONNECTED_BIT );
			break;

    	case SYSTEM_EVENT_AP_STADISCONNECTED:
    		ESP_LOGI(tag, "station:"MACSTR" leave,AID=%d", MAC2STR(event->event_info.sta_disconnected.mac), event->event_info.sta_disconnected.aid);
    		xEventGroupClearBits( wifi_sta_event_group, WIFI_CONNECTED_BIT );
			break;

		default:
			ESP_LOGI( tag, "default!" );
			break;
	}
	return ESP_OK;
}

//
static void wifi_init_sta( void )
{
	wifi_sta_event_group = xEventGroupCreate();

    nvs_flash_init();
    tcpip_adapter_init();

    ESP_ERROR_CHECK( esp_event_loop_init( event_handler_sta, NULL ) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    wifi_config_t wifi_config ;
	memcpy((char*)&wifi_config.sta.ssid, (char*)&DevReg.SSID_WiFi, sizeof(DevReg.SSID_WiFi) );
	memcpy((char*)&wifi_config.sta.password, (char*)&DevReg.Password_Wifi, sizeof(DevReg.Password_Wifi) );
	wifi_config.sta.bssid_set = 0;
	ESP_LOGI(tag,"\n\n SSID : %s \n PSWD :%s \n\n",(char*)&wifi_config.sta.ssid, (char*)&wifi_config.sta.password);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_LOGI( tag, "wifi_init_sta finished!" );
}
/* ---------------------------------------------------------------------------------------- */

//
static void wifi_reinit( void )
{
	wifi_sta_event_group = xEventGroupCreate();

	nvs_flash_init();
	tcpip_adapter_init();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
	wifi_config_t wifi_config ;
	memcpy((char*)&wifi_config.sta.ssid, (char*)&DevReg.SSID_WiFi, sizeof(DevReg.SSID_WiFi) );
	memcpy((char*)&wifi_config.sta.password, (char*)&DevReg.Password_Wifi, sizeof(DevReg.Password_Wifi) );
	wifi_config.sta.bssid_set = 0;
	ESP_LOGI(tag,"\n\n SSID : %s \n PSWD :%s \n\n",(char*)&wifi_config.sta.ssid, (char*)&wifi_config.sta.password);
	ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
	ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
	ESP_ERROR_CHECK( esp_wifi_start() );
	ESP_LOGI( tag, "wifi_init_sta finished!" );
}

/* ---------------------------------------------------------------------------------------- */

//
static void dev_reg_init( void )
{
	uint32_t i, j;

	DevReg.PowerDownTimeout 	= DEF_PWDN_15MIN_TIMEOUT;

	DevReg.CardType 			= CARD_TYPE;

	memcpy( &DevReg.SerialNumber_WiFi, card_param.DefSerialNo, sizeof( DevReg.SerialNumber_WiFi ) );
	for( i=0; i<4; i++ ) { DevReg.SerialNumber_WiFi[ i ] = 0; }

	for( i=0; i<WLAN_SSID_LEN; i++ ) { DevReg.SSID_WiFi[ i ] = 0; DevReg.Password_Wifi[ i ] = 0; }
	strcpy((char *)DevReg.SSID_WiFi,		(char *)MAIN_WLAN_SSID);
	strcpy((char *)DevReg.Password_Wifi,	(char *)MAIN_WLAN_PSK);

	DevReg.Trigger				= DEF_MS;
	DevReg.SampleFrequency		= DEF_FS;
	DevReg.NumberOfSamples		= DEF_NOS;
	DevReg.NumberOfChannels 	= DEF_NOCH;
	DevReg.SampleFrequency		= FS_8192;
	DevReg.Fram_version 		= DEV_REG_REFRESH;

	float range_min_tmp[]		= DEF_RANGE_MIN;
	float range_max_tmp[]		= DEF_RANGE_MAX;
	uint8_t range_idx_tmp[]		= DEF_RANGE_IDX;
	float sensor_sens_tmp[]		= { (SensorSens.sens_x * ADC_SENS), ( SensorSens.sens_z * ADC_SENS ), ( 0.05 * PDM_SENS ), ( SensorSens.sens_acce * ACCE_SENS ), ( SensorSens.sens_acce * ACCE_SENS ), ( SensorSens.sens_acce * ACCE_SENS ), ( SensorSens.sens_gyro * GYRO_SENS), ( SensorSens.sens_gyro * GYRO_SENS), ( SensorSens.sens_gyro * GYRO_SENS) };
	uint8_t unit_tmp[]			= DEF_UNIT;
	uint8_t sensor_type_tmp[]	= DEF_SENSOR_TYPE;

	for( i=0; i<MAX_NOCH; i++ )
	{
		DevReg.ChSequence[ i ]				= i;
		DevReg.ChType[ i ]					= sensor_type_tmp[ i ];
		DevReg.ChRangeMin[ i ]				= range_min_tmp[ i ];
		DevReg.ChRangeMax[ i ]				= range_max_tmp[ i ];
		DevReg.ChRangeIdx[ i ]				= range_idx_tmp[ i ];
		DevReg.ChSensorSens[ i ]			= sensor_sens_tmp[ i ];
		DevReg.ChUnit[ i ]					= unit_tmp[ i ];
		DevReg.ChSensorType[ i ]			= sensor_type_tmp[ i ];
		DevReg.ChSensorTypeSequence[ i ]	= sensor_type_tmp[ i ];

		for( j=0; j<NOVR; j++ )
		{
			DevReg.ChCalGainInt[ j ][ i ] 	= card_param.FabChGainInt[ j ][ i ];
			DevReg.ChCalOffsetInt[ j ][ i ] = card_param.FabChOffsetInt[ j ][ i ];
		}
	}

	DevReg.Saved_RCOMP0 = 0;
	DevReg.Saved_TempCo = 0 ;
	DevReg.Saved_FullCapRep = 0;
	DevReg.Saved_Cycles = 0 ;
	DevReg.Saved_FullCapNom = 0;
}

static void sensor_sens_init( void )
{
	SensorSens.sens_z = ADC_Z_MULT;
	SensorSens.sens_x = ADC_X_MULT;
	SensorSens.sens_acce = ACCE_MULT;
	SensorSens.sens_gyro = GYRO_MULT;
	SensorSens.Sensor_Sens[0] =  ( SensorSens.sens_z * ADC_SENS );
	SensorSens.Sensor_Sens[1] =  ( SensorSens.sens_x * ADC_SENS );
	SensorSens.Sensor_Sens[2] =  ( 0.05 * PDM_SENS );
	SensorSens.Sensor_Sens[3] =  ( SensorSens.sens_acce * ACCE_SENS );
	SensorSens.Sensor_Sens[4] =  ( SensorSens.sens_acce * ACCE_SENS );
	SensorSens.Sensor_Sens[5] =  ( SensorSens.sens_acce * ACCE_SENS );
	SensorSens.Sensor_Sens[6] =  ( SensorSens.sens_gyro * GYRO_SENS);
	SensorSens.Sensor_Sens[7] =  ( SensorSens.sens_gyro * GYRO_SENS);
	SensorSens.Sensor_Sens[8] =  ( SensorSens.sens_gyro * GYRO_SENS);
	SensorSens.serial_no = DEVICE_SN;
	SensorSens.button_sleep = 0xffff;
	SensorSens.button_time = 0x4000;
}
//
static void dev_measurement_remote_mvibe( int sockIdx )
{
	static uint32_t HeaderBuffer[32];
	msg_base_t MsgBase;
	uint32_t recvLen = 0;

	if ( DevState.MeasurementRemoteState == REMOTE_MEASURE_START )
	{
		Flags_Wraper.flag = 1;

		if(led_flag == 1){led( 0,255,0, 0 );}
		switch( DevState.MeasurementParam )
		{
			// -------------- Analog and Digital ------------- //
			case PARAM_ANALOG_DIGITAL:
			{
				MsgBase.m_key = CMD_KEY;
				MsgBase.m_cmd = cmdMeasuredData;
				MsgBase.m_cmdParam = DevState.MeasurementMode + DevState.MeasurementParam;

				MsgBase.m_cmdDataSize = 4;							// add IOP, FS, POS, NOCh
				MsgBase.m_cmdDataSize += DevReg.NumberOfChannels;	// add channels sequence size
				MsgBase.m_cmdDataSize *= sizeof(uint32_t);			// all data are 32-bit
				MsgBase.m_cmdDataSize += DevState.PackSizeSample;

				ESP_LOGI(tag," REMOTE_MEASURE_START - PARAM_ANALOG_DIGITAL ( fs:%dHz, PoSa:%d )\n", DevReg.SampleFrequency, DevState.PackOfSamples );
			}
			break;

			case PARAM_COUNTER_DATA:
			{

				MsgBase.m_key = CMD_KEY;
				MsgBase.m_cmd = cmdMeasuredData;
				MsgBase.m_cmdParam = DevState.MeasurementMode + DevState.MeasurementParam;

				MsgBase.m_cmdDataSize = 4;							// add IOP, FS, POS, NOCh
				MsgBase.m_cmdDataSize += DevReg.NumberOfChannels;	// add channels sequence size
				MsgBase.m_cmdDataSize *= sizeof(uint32_t);			// all data are 32-bit
				MsgBase.m_cmdDataSize += DevState.PackSizeSample;

				ESP_LOGI(tag," REMOTE_MEASURE_START - PARAM_COUNTER_DATA ( fs:%dHz, PoSa:%d )\n", DevReg.SampleFrequency, DevState.PackOfSamples );
			}
			break;

			// ------------------ Unknown ------------------ //
			default:
			{
				add_error( ErrCnt, errMeasurementMode );
			}
		}

		if ( ErrCnt == 0 )
		{
			// ---------- //
			DevState.PackIndex = 0;

			HeaderBuffer[0] = MsgBase.m_key;
			HeaderBuffer[1] = MsgBase.m_cmd;
			HeaderBuffer[2] = MsgBase.m_cmdParam;
			HeaderBuffer[3] = MsgBase.m_cmdDataSize;
			HeaderBuffer[4] = DevState.PackIndex;
			HeaderBuffer[5] = DevReg.SampleFrequency;
			HeaderBuffer[6] = DevState.PackOfSamples;
			HeaderBuffer[7] = DevReg.NumberOfChannels;

			for(int i=0; i<DevReg.NumberOfChannels; i++ ) { HeaderBuffer[8+i] = DevReg.ChSequence[i]; }

			DevAcquire.HeaderBufferSent = 0;
			DevAcquire.SamplesCntSent = 0;

			MsgBase.m_key = CMD_KEY;
			MsgBase.m_cmd = cmdScanSensorsReady;
			MsgBase.m_cmdParam = 0;
			MsgBase.m_cmdDataSize = 0;

			send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, (uint8_t*)&MsgBase, sizeof(MsgBase) );

			DevState.MeasurementRemoteState = REMOTE_MEASURE_IN_PROGRESS;

		}
		else
		{
			send_error_buffer( sockIdx );
			DevState.MeasurementRemoteState = REMOTE_MEASURE_IDLE;
		}
	}

// ================================================================= //
	if ( DevState.MeasurementRemoteState == REMOTE_MEASURE_IN_PROGRESS )
	{
		ESP_LOGI(tag," REMOTE_MEASURE_IN_PROGRESS, PackIdx: %d \n", DevState.PackIndex );

		// --------------- Continuous Mode --------------- //
		if ( DevState.MeasurementMode == PARAM_CONTINUOUS_MODE )
		{
			if ( mySockReg[ sockIdx ].acceptedSocketHdl >= 0 )
			{
				if ( DevAcquire.HeaderBufferSent == 0 )
				{
					if( DevState.PackIndex == 0)
					{
						for( int i = 0; i < 32; i++) // clean first probe
						{
							multichannel_read_samples( buffer, &recvLen );
						}
					}
					HeaderBuffer[4] = DevState.PackIndex++;
					send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, (uint8_t*)HeaderBuffer, DevState.PackSizeHeader );
					DevAcquire.HeaderBufferSent = 1;
				}

				if ( DevAcquire.HeaderBufferSent == 1 )
				{
					uint32_t totalPackSize32 = DevState.PackOfSamples * DevReg.NumberOfChannels;
					uint32_t totalPackToSend = DevReg.NumberOfChannels * PACK_OF_ONE_CH;

					if( DevState.PackIndex > 0 )
					{
						while ( totalPackSize32 >= totalPackToSend )
						{
							multichannel_read_samples( buffer, &recvLen );

							if(recvLen == totalPackToSend)
							{
							send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, (uint8_t*)buffer, totalPackToSend * sizeof(uint32_t) );
							}

							totalPackSize32 -= totalPackToSend;
						}
					}

					DevAcquire.SamplesCntSent += DevState.PackOfSamples;
					DevAcquire.HeaderBufferSent = 0;
			}
		  }
		}
	}

	// ================================================================= //
	if (DevState.MeasurementRemoteState == REMOTE_MEASURE_WAIT_TO_STOP)
	{
		ESP_LOGI(tag," REMOTE_MEASURE_WAIT_TO_STOP \n");
		MsgBase.m_key = CMD_KEY;
		MsgBase.m_cmd = cmdScanSensorsStopped;
		MsgBase.m_cmdParam = 0;
		MsgBase.m_cmdDataSize = 0;
		int err = send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, (uint8_t*)&MsgBase, sizeof(MsgBase) );
		ESP_LOGI(tag,"\n\n ERR %d, MSG CMD %d \n\n", err, MsgBase.m_cmd);
		DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;
	}

	// ================================================================= //
	if (DevState.MeasurementRemoteState == REMOTE_MEASURE_STOPPED)
	{
		if(led_flag == 1){ led( 0,0,255, 0 );}
		ESP_LOGI(tag," REMOTE_MEASURE_STOPPED \n");
		DevState.MeasurementRemoteState = REMOTE_MEASURE_IDLE;
	}

	// ================================================================= //
	if (DevState.MeasurementRemoteState == REMOTE_MEASURE_IDLE)
	{
	}
}

/* ---------------------------------------------------------------------------------------- */

//
static void udp_response_msg_init( void )
{
	uint32_t i, j;

	msgUDP.header.m_key				= CMD_KEY;
	msgUDP.header.m_cmd 			= cmdCardParameters;
	msgUDP.header.m_cmdParam		= 1;
	msgUDP.header.m_cmdDataSize 	= sizeof(DevReg.SerialNumber_WiFi) + sizeof(card_param_udp_t);

	msgUDP.serialNo[0]				= DevReg.SerialNumber_WiFi[0];
	msgUDP.serialNo[1]				= DevReg.SerialNumber_WiFi[1];
	msgUDP.serialNo[2]				= DevReg.SerialNumber_WiFi[2];
	msgUDP.serialNo[3]				= DevReg.SerialNumber_WiFi[3];
	msgUDP.serialNo[4]				= DEV_TCP_PORT;
	msgUDP.serialNo[5]				= SensorSens.serial_no;

	msgUDP.cp.DefCardType			= card_param.DefCardType;
	msgUDP.cp.MaxNumberOfSamples	= card_param.MaxNumberOfSamples;
	msgUDP.cp.MaxNumberOfChannels	= card_param.MaxNumberOfChannels;
	msgUDP.cp.NoVChT				= card_param.NoVChT;
	msgUDP.cp.NoVFS					= card_param.NoVFS;
	msgUDP.cp.NoVR					= card_param.NoVR;
	msgUDP.cp.NoSensor 				= card_param.NoSensor;
	msgUDP.cp.SensorIntExtSw		= card_param.SensorIntExtSw;

	for ( i = 0; i < NOVCHT; i++ )
	{
		for ( j = 0; j < MAX_NOCH; j++ )
		{
			msgUDP.cp.DefChannelType[ i ][ j ] = card_param.DefChannelType[ i ][ j ];
		}
	}

	for ( i = 0; i < NOVFS; i++ )
	{
		msgUDP.cp.DefValidFS[ i ] = card_param.DefValidFS[ i ];
	}

	for ( i = 0; i < ( NOVR * 2 ); i++ )
	{
		for ( j = 0; j < MAX_NOCH; j++ )
		{
			memcpy( (uint8_t*)&msgUDP.cp.DefValidRange[ i ][ j ], (uint8_t*)&card_param.DefValidRange[ i ][ j ], sizeof( float ) );
		}
	}

	for ( i = 0; i < NOSENSORS; i++ )
	{
		memcpy( (uint8_t*)&msgUDP.cp.DefSensorSens[ i ], (uint8_t*)&SensorSens.Sensor_Sens[ i ], sizeof( float ) );
	}

	for ( i = 0; i < ( NOSENSORS * 2 ); i++ )
	{
		memcpy( (uint8_t*)&msgUDP.cp.DefSensorFreqRange[ i ], (uint8_t*)&card_param.DefSensorFreqRange[ i ], sizeof( float ) );
	}
}

//
static void dev_udp_server_mvibe( int sockIdx )
{
	msg_base_t	MsgBase;

	if ( mySockReg[ sockIdx ].recvLen > 0 )							// check RX data
	{
		uint32_t *pBuffer 	= (uint32_t*)gau8SocketBufferUDP;
		MsgBase.m_cmd 		= *pBuffer++;						// read command
		MsgBase.m_cmdParam 	= *pBuffer;							// read parameter

		int i = 0;
		int j = 0;

		if ( ( MsgBase.m_cmd == cmdCardParametersRead ) && ( MsgBase.m_cmdParam == 1 ) )			// select command
		{
			msgUDP.serialNo[ 0 ] = DevReg.SerialNumber_WiFi[ 0 ];
			msgUDP.serialNo[ 1 ] = DevReg.SerialNumber_WiFi[ 1 ];
			msgUDP.serialNo[ 2 ] = DevReg.SerialNumber_WiFi[ 2 ];
			msgUDP.serialNo[ 3 ] = DevReg.SerialNumber_WiFi[ 3 ];

			for( i = 0; i < NOVR; i++ )
			{
				for( j = 0; j < MAX_NOCH; j++ )
				{
					DevReg.ChSensorSens[ j ] = (SensorSens.Sensor_Sens[ j ]) * (DevReg.ChCalGainInt[ i ][ j ]);
					ESP_LOGI(tag,"\n\n CHSens : %4.1f \n\n", (float)DevReg.ChSensorSens[ j ]);
					memcpy( (uint8_t*)&msgUDP.cp.DefSensorSens[ j ], (uint8_t*)&DevReg.ChSensorSens[ j ], sizeof( float ) );
				}
			}

			sendto( mySockReg[ sockIdx ].listenSocketHdl, (void *)&msgUDP, sizeof(msgUDP), 0, (struct sockaddr *)(&mySockReg[ sockIdx ].socketAddr), sizeof(mySockReg[ sockIdx ].socketAddr) );
		}
	}
}

//
static void dev_tcp_server_mvibe( int sockIdx )
{
	static rx_state_t	RxState;
	static msg_base_t 	MsgBase;
	static uint32_t		MsgCmdTmp;
	static uint32_t		ulTmp, ulLenPart8, ulLenPart32;				// temporal length of data to send or received
	uint32_t			i, j, k, ulTmpX, valVaild;					// temporal variable
	float				fTmp;
	float				calSetGain[ NOVR ][ MAX_NOCH ], calSetOffset[ NOVR ][ MAX_NOCH ];
	uint32_t			*p32Buffer;
	static uint32_t		*p32RxBufTcp;								// pointer to TCP/IP Receiver buffer
	device_register_t	DevRegTmp;


	/* *********************** RECEIVE ************************** */

	if ( mySockReg[ sockIdx ].FlagCheckSocketTcp == true )
	{
		ESP_LOGI(tag,"\n\n RECEIVE \n\n ");
		RxState.cmd_state = RX_IDLE_STATE;
		mySockReg[ sockIdx ].FlagCheckSocketTcp = false;
	}

	MsgBase.m_cmd = cmdDummy;


	uint32_t ulLen8 = mySockReg[ sockIdx ].recvLen;

	if ( ulLen8 > 0 )
	{
		p32Buffer = (uint32_t*)gau8SocketBuffer;

		switch ( RxState.cmd_state )
		{
			/* --- 1st reads of received data --- */
			case RX_IDLE_STATE:
			{
				if ( ulLen8 % sizeof(uint32_t) )					// check align to 32bit
				{
					ESP_LOGI(tag,"\n\n\n\n RX_IDLE_STATE \n\n\n");
					add_error( ErrCnt, errRX );
					break;
				}

				MsgBase.m_key = CMD_KEY;

				ulLen8 -= sizeof(MsgBase);
				uint32_t ulLen32 = ulLen8 / sizeof(uint32_t);

				for ( i = 0; i <= ulLen32; )						// check data for message
				{
					if ( memcmp( (uint8_t*)p32Buffer, (uint8_t*)&MsgBase.m_key, sizeof(MsgBase.m_key) ) == 0 )
					{
						ulLenPart8 = ulLen8 - i * sizeof(uint32_t);	// size of received valid data
						ulLenPart32 = ulLenPart8 / sizeof(uint32_t);

						p32Buffer++;
						MsgBase.m_cmd = *p32Buffer;					// read command
						p32Buffer++;
						MsgBase.m_cmdParam = *p32Buffer;			// read parameter
						p32Buffer++;
						MsgBase.m_cmdDataSize = *p32Buffer;			// read data size
						p32Buffer++;

						ESP_LOGI(tag," mVibe: new command receive: 0x%02x \n", MsgBase.m_cmd );

						if ( ( MsgBase.m_cmdDataSize + sizeof(MsgBase) ) > MAX_TCP_BUFF_SIZE )
						{
							add_error(ErrCnt,errRxOverflow);
							break;
						}

						p32RxBufTcp = (uint32_t*)recvCmdBuff;
						memcpy( p32RxBufTcp, p32Buffer, ulLenPart8 );

						if ( ulLenPart8 < MsgBase.m_cmdDataSize )
						{
							RxState.cmd_state = RX_CMD_PART_RECEIVED;
							MsgCmdTmp = MsgBase.m_cmd;
							MsgBase.m_cmd = cmdDummy;
							p32RxBufTcp += ulLenPart32;				// set pointer at the end of partial data
						}

						break;
					}
					else
					{
						i++;
						p32Buffer++;
					}
				}
			}
			break;

			/* --- n-th reads of received data --- */
			case RX_CMD_PART_RECEIVED:
			{
				if ( (uint32_t)p32RxBufTcp - (uint32_t)recvCmdBuff + ulLen8 > MAX_TCP_BUFF_SIZE )
				{
					ESP_LOGI(tag,"\n\n\n\n RX_CMD_PART_RECEIVED \n\n\n");
					add_error(ErrCnt,errRxOverflow);
					break;
				}

				memcpy( p32RxBufTcp, p32Buffer, ulLen8 );
				ulLenPart8 += ulLen8;
				if ( ulLenPart8 < MsgBase.m_cmdDataSize )
				{
					RxState.cmd_state = RX_CMD_PART_RECEIVED;
					p32RxBufTcp += ulLen8 / sizeof(uint32_t);		// set pointer at the end of partial data
				}
				else
				{
					RxState.cmd_state = RX_IDLE_STATE;
					MsgBase.m_cmd = MsgCmdTmp;
					p32RxBufTcp = (uint32_t*)recvCmdBuff;			// set pointer at the beginning of data
				}

			}
			break;
		}

		if ( ErrCnt != 0 ) { MsgBase.m_cmd = cmdError; RxState.cmd_state = RX_IDLE_STATE; }
	}

	/* **************************************************************** */
	/* ********************** SELECT COMMAND ************************** */
	/* **************************************************************** */

	switch ( MsgBase.m_cmd )													// if receive command -> SELECT COMMAND
	{
		/* *********************** CONFIGURATION ************************** */
		/* **************************************************************** */
		case cmdConfigurationSet:
		{
			DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;
			memcpy( (uint8_t*)&DevRegTmp, (uint8_t*)&DevReg, sizeof( DevRegTmp ) );

			// Card Type
			DevRegTmp.CardType = *p32RxBufTcp;
			if ( DevRegTmp.CardType != CARD_TYPE ) { add_error(ErrCnt,errCardType); }
			p32RxBufTcp++;

			// Ethernet IP address (ignored) and Serial Number
			p32RxBufTcp += 5;
			DevRegTmp.SerialNumber_WiFi[ 5 ] = *p32RxBufTcp;
			if ( DevRegTmp.SerialNumber_WiFi[ 5 ] != card_param.DefSerialNo[ 5 ] ) {add_error(ErrCnt,errSerialNo); }
			p32RxBufTcp++;

			// Trigger Card
			ulTmp = *p32RxBufTcp;
			if ( ulTmp <= MAX_DEV_TRIG ) { DevRegTmp.Trigger = (uint8_t)ulTmp; }
			else {add_error(ErrCnt,errTrigger); }
			p32RxBufTcp++;

			// Sample Frequency
			DevRegTmp.SampleFrequency = *p32RxBufTcp;
			for( k = 0; k < NOVFS; k++ )
			{
				if ( DevRegTmp.SampleFrequency != card_param.DefValidFS[ k ] ) { valVaild = false; }
				else { valVaild = true; break; }
			}
			if ( valVaild == false )
			{
				if ( DevRegTmp.SampleFrequency < card_param.DefValidFS[ 0 ] ) { add_error(ErrCnt,errFsLow); }
				if ( DevRegTmp.SampleFrequency > card_param.DefValidFS[ NOVFS - 1 ] ) { add_error(ErrCnt,errFsHigh); }
				ulTmpX = card_param.DefValidFS[ NOVFS - 1 ] % DevRegTmp.SampleFrequency;
				if ( ulTmpX != 0 ) { add_error(ErrCnt,errFsApprox); }
			}
			p32RxBufTcp++;

			// Number of Samples
			DevRegTmp.NumberOfSamples = *p32RxBufTcp;
			if (( DevRegTmp.NumberOfSamples == 0 ) || ( DevRegTmp.NumberOfSamples > MAX_NOS )) { add_error(ErrCnt,errNOS); }
			p32RxBufTcp++;

			// Number of Channels
			ulTmp = *p32RxBufTcp;
			if ( (ulTmp == 0) || (ulTmp > MAX_NOCH) ) { add_error(ErrCnt,errNOCH); }
			else
			{
				DevRegTmp.NumberOfChannels = ulTmp;

				for( i = 0; i < MAX_NOCH; i++ ) { DevRegTmp.ChSequence[ i ] = 0; DevRegTmp.ChSensorTypeSequence[ i ] = DEV_IN_DIG; }	// reset used channels sequence

				for( i = 0; i < DevRegTmp.NumberOfChannels; i++ )
				{
					p32RxBufTcp++;									// pointer at [ChSequence]
					ulTmpX  = *p32RxBufTcp;
					if ( ulTmpX >= MAX_NOCH ) { add_error(ErrCnt,errWrongChannel);	break; }

					DevRegTmp.ChSequence[ i ] = ulTmpX;

					j = DevRegTmp.NumberOfChannels;					// register offset

					p32RxBufTcp += j;								// pointer go to [ChannelRangeMin]
					memcpy( &fTmp, p32RxBufTcp, sizeof(fTmp) );
					for( k = 0; k < NOVR; k++ )
					{
						if ( *((int*)(&fTmp)) != *((int*)(&card_param.DefValidRange[ k * 2 ][ DevRegTmp.ChSequence[ i ] ])) ) { valVaild = false; }
						else { valVaild = true; DevRegTmp.ChRangeMin[ ulTmpX ] = fTmp; break; }
					}
					if ( valVaild == false ) { add_error(ErrCnt,errRange); break; }

					p32RxBufTcp += j;								// pointer go to [ChannelRangeMax]
					memcpy( &fTmp, p32RxBufTcp, sizeof(fTmp) );
					for( k = 0; k < NOVR; k++ )
					{
						if ( *((int*)(&fTmp)) != *((int*)&card_param.DefValidRange[ k * 2 + 1 ][ DevRegTmp.ChSequence[ i ] ]) )
						{
							valVaild = false;
						}
						else
						{
							valVaild = true;
							DevRegTmp.ChRangeMax[ ulTmpX ] = fTmp;
							DevRegTmp.ChRangeIdx[ ulTmpX ] = k;
							break;
						}
					}
					if ( valVaild == false ) { add_error(ErrCnt,errRange); break; }

					p32RxBufTcp += j;								// pointer go to [ChannelSensorSensitivity]
					if ( MsgBase.m_cmdParam == paramUseAllParam || MsgBase.m_cmdParam == paramUseAllParamAndSave )
					{
						memcpy( &DevRegTmp.ChSensorSens[ ulTmpX ], p32RxBufTcp, sizeof(fTmp) );
						if (  *((int*)(&DevRegTmp.ChSensorSens[ ulTmpX ] )) == 0 ) { add_error(ErrCnt,errSensorSens); break; }
					}
					else
					{
						DevRegTmp.ChSensorSens[ ulTmpX ] = SensorSens.Sensor_Sens[ ulTmpX ];		// TEMPORARY
					}

					p32RxBufTcp += j;								// pointer go to [ChannelGain]

					p32RxBufTcp += j;								// pointer go to [ChannelOffset]

					p32RxBufTcp += j;								// pointer go to [ChannelUnit]
					ulTmp = *p32RxBufTcp;
					if ( ulTmp <= MAX_UNIT ) { DevRegTmp.ChUnit[ ulTmpX ] = ulTmp; }
					else { add_error(ErrCnt,errChUnit); break; }

					calculate_unit_bbox_ver_1( ulTmpX );			// compute [ChAccelUnity]

					p32RxBufTcp += j;								// pointer go to [SensorType]
					ulTmp = *p32RxBufTcp;
					if ( ulTmp <= MAX_DEV_INPUT )
					{
						DevRegTmp.ChSensorType[ ulTmpX ] = (uint8_t)ulTmp;
						DevRegTmp.ChSensorTypeSequence[ i ] = (uint8_t)ulTmp;
					}
					else { add_error(ErrCnt,errSensorType); break; }

					j = DevRegTmp.NumberOfChannels * 7;
					p32RxBufTcp -= j;								// return to the next [ChSequence]
				}

				p32RxBufTcp += DevRegTmp.NumberOfChannels * 7 + 1;
				ulTmp = *p32RxBufTcp;

				if ( ulTmp == SENSOR_INTERNAL ) { DevRegTmp.SensorIntExtSw = ulTmp; }//KK
				else { add_error(ErrCnt,errSensIntExtSw); }
			}

			if ( ErrCnt == 0 )									// if no any errors
			{
				memcpy( (uint8_t*)&DevReg, (uint8_t*)&DevRegTmp, sizeof( DevReg ) );

				DevState.ConfigState = CONFIG_SET;

				measure_flag = 1;
				Flags_Wraper.err_wraper = 0;
				vTaskDelay(1);

				pcm_config();

				if( Flags_Wraper.err_wraper == 0 )
				{
				MsgBase.m_key = CMD_KEY;
				MsgBase.m_cmd = cmdConfigurationDone;
				MsgBase.m_cmdParam = 0;
				MsgBase.m_cmdDataSize = 0;

				send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, (uint8_t*)&MsgBase, sizeof(MsgBase) );
				}
				else
				{
					add_error(ErrCnt, errMeasurementMode);
				}
			}
		}
		break;

		/* **************************************************************** */
		/* ************************ SCAN SENSORS ************************** */
		/* **************************************************************** */
		case cmdScanSensorsStart:
		{
			if ( DevState.ConfigState == CONFIG_NOSET ) {add_error(ErrCnt,errNoConfig);}


			ESP_LOGI(tag,"\n\n\n\n DevState.MeasurementRemoteState %d \n\n\n\n\n",DevState.MeasurementRemoteState);
			if ( DevState.MeasurementRemoteState == REMOTE_MEASURE_IDLE )
			{
				switch( MsgBase.m_cmdParam & MaskParamData )
				{
					// -------------- Analog and Digital ------------- //
					case PARAM_ANALOG_DIGITAL:
					{
						DevState.MeasurementParam = PARAM_ANALOG_DIGITAL;
					}
					break;

					// -------------- Test Counter Data -----------_-- //
					case PARAM_COUNTER_DATA:
					{
						DevState.MeasurementParam = PARAM_COUNTER_DATA;
					}
					break;

					// -------------------- Unknown ------------------ //
					default:
					{
						add_error(ErrCnt,errMeasurementMode);
					}
				}

				switch( MsgBase.m_cmdParam & MaskParamMode )
				{
					// --------------- Continuous Mode --------------- //
					case PARAM_CONTINUOUS_MODE:
					{
						ulTmp = *p32RxBufTcp;			// Pack Of Samples
						if ( (ulTmp == 0)||(ulTmp > MAX_POS) ) {add_error(ErrCnt,errPackOfSamples);}
						else
						{
							DevState.MeasurementMode = PARAM_CONTINUOUS_MODE;
							DevState.PackOfSamples = ulTmp;
						}
					}
					break;

					// --------------- Unknown Mode --------------- //
					default:
					{
						add_error(ErrCnt,errMeasurementMode);
					}
				}
			}
			else
			{
				add_error(ErrCnt,errDevUnderRemoteMeasure);
			}

			if (ErrCnt == 0)
			{
				DevState.PackSizeHeader = (8 + DevReg.NumberOfChannels) * sizeof(uint32_t);
				DevState.PackSizeSample = DevState.PackOfSamples * DevReg.NumberOfChannels * sizeof(float);

				DevState.MeasurementRemoteState = REMOTE_MEASURE_START;
			}

		}
		break;

		// ================================================================= //
		case cmdScanSensorsStop:
		{
			switch( MsgBase.m_cmdParam & MaskParamMode )
			{
				// --------------- Continuous Mode --------------- //
				case PARAM_CONTINUOUS_MODE:
				{
					DevState.ConfigState = CONFIG_NOSET;
					ESP_LOGI(tag,"\n\n CMD STOP \n\n");
					measure_flag = 0;
					sleep_pcm();
					DevState.MeasurementRemoteState = REMOTE_MEASURE_WAIT_TO_STOP;
				}
				break;

				// ----------------- Unknown Mode ---------------- //
				default:
				{
					add_error(ErrCnt,errMeasurementMode);
				}
			}
		}
		break;

		/* **************************************************************** */
		/* ************************ WIFI SETTINGS ************************* */
		/* **************************************************************** */
		case cmdWiFiSettingsSet:
		{
			MsgBase.m_key = CMD_KEY;
			MsgBase.m_cmd = cmdWiFiSettingsDone;
			MsgBase.m_cmdParam = 0;
			MsgBase.m_cmdDataSize = 64;

			memcpy( (char*)&DevReg.SSID_WiFi, (char*)p32RxBufTcp, sizeof(DevReg.SSID_WiFi) );
			p32RxBufTcp += sizeof(DevReg.SSID_WiFi)/sizeof(uint32_t);
			memcpy( (char*)&DevReg.Password_Wifi, (char*)p32RxBufTcp, sizeof(DevReg.Password_Wifi) );
			p32RxBufTcp += sizeof(DevReg.Password_Wifi)/sizeof(uint32_t);

			ESP_LOGI(tag,"\n\n mVibe: new SSID: %s \n mVibe: new PSWD: %s \n\n", DevReg.SSID_WiFi, DevReg.Password_Wifi );

			memcpy( &ucTxBuf[0], (uint8_t*)&MsgBase, sizeof(MsgBase) );

			send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, &ucTxBuf[0], sizeof(MsgBase) + MsgBase.m_cmdDataSize );

			vTaskDelay(100);

			esp_wifi_disconnect();

			esp_wifi_stop();

			esp_wifi_deinit();

			vTaskDelay(100);

			wifi_reinit();


		}
		break;

		/* **************************************************************** */
		/* *********************** cmdCalibrationSet ********************** */
		/* **************************************************************** */
		case cmdCalibrationSet:
			{
				MsgBase.m_key = CMD_KEY;
				MsgBase.m_cmd = cmdCalibrationSetDone;
				MsgBase.m_cmdParam = 0;
				MsgBase.m_cmdDataSize = 72;

				if(MsgBase.m_cmdParam == 1)
				{
					for( i=0; i<NOVR; i++ )
					{
						for(j=0; j<MAX_NOCH; j++)
						{
						memcpy( (float*)&DevReg.ChCalGainInt[ i ][ j ], (float*)p32RxBufTcp, sizeof(DevReg.ChCalGainInt[ i ][ j ]));

						calSetGain[i][j] = DevReg.ChCalGainInt[i][j];

						p32RxBufTcp++;

						ESP_LOGI(tag,"\n\n CalGain1 :%4.1f  \n\n SETGAI : %4.1f \n\n", (float)DevReg.ChCalGainInt[ i ][ j ], (float) calSetGain[i][j]);
						}
					}


					for( i=0; i<NOVR; i++ )
					{
						for(j=0; j<MAX_NOCH; j++)
						{
						memcpy( (float*)&DevReg.ChCalOffsetInt[ i ][ j ], (float*)p32RxBufTcp, sizeof(DevReg.ChCalOffsetInt[ i ][ j ]));

						calSetOffset[i][j] = DevReg.ChCalOffsetInt[i][j];

						p32RxBufTcp++;

						ESP_LOGI(tag,"\n\n CalOffset1 :%ld  \n\n  SETOF: %ld \n\n", (long int)DevReg.ChCalOffsetInt[ i ][ j ], (long int) calSetOffset[i][j]);


						}
					}

					memcpy( &ucTxBuf[0], (uint8_t*)&MsgBase, sizeof(MsgBase) );

					memcpy( &ucTxBuf[16], (float*)&calSetGain, sizeof(calSetGain));

					memcpy( &ucTxBuf[72], (float*)&calSetOffset, sizeof(calSetOffset));

					send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, &ucTxBuf[0], sizeof(MsgBase) + MsgBase.m_cmdDataSize );
					ESP_LOGI(tag, "\n\n msgbase %d \n\n ",MsgBase.m_cmdParam);

				}
				else if(MsgBase.m_cmdParam == 0)
				{
					memcpy( &ucTxBuf[0], (uint8_t*)&MsgBase, sizeof(MsgBase) );

					memcpy( &ucTxBuf[16], (float*)&DevReg.ChCalGainInt, sizeof(DevReg.ChCalGainInt));

					memcpy( &ucTxBuf[72], (float*)&DevReg.ChCalOffsetInt, sizeof(DevReg.ChCalGainInt));

					send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, &ucTxBuf[0], sizeof(MsgBase) + MsgBase.m_cmdDataSize );
					ESP_LOGI(tag, "\n\n msgbase %d \n\n ",MsgBase.m_cmdParam);
				}

			}
			break;

		/* **************************************************************** */
		/* *********************** BATTERY STATUS ************************* */
		/* **************************************************************** */
		case cmdBatteryStatusRead:
		{
			MsgBase.m_key = CMD_KEY;
			MsgBase.m_cmd = cmdBatteryStatusDone;
			MsgBase.m_cmdDataSize = 68;

			memcpy( &ucTxBuf[0], (uint8_t*)&MsgBase, sizeof(MsgBase) );

			ulTmp = FIRMWARE_VER;
			memcpy( &ucTxBuf[16], (uint8_t*)&ulTmp, sizeof(uint32_t) );		// Status
			fTmp = FuelReg.BatteryVoltage;
			memcpy( &ucTxBuf[20], (uint8_t*)&fTmp, sizeof(fTmp) );		// Voltage [V]
			fTmp = FuelReg.BatteryCurrent;
			memcpy( &ucTxBuf[24], (uint8_t*)&fTmp, sizeof(fTmp) );		// Current
			fTmp = 0;
			memcpy( &ucTxBuf[28], (uint8_t*)&fTmp, sizeof(fTmp) );		// CurrentAvg
			fTmp = FuelReg.BatteryTemperature;
			memcpy( &ucTxBuf[32], (uint8_t*)&fTmp, sizeof(fTmp) );		// Temperature [deg. C]
			fTmp = FuelReg.RAAC;
			memcpy( &ucTxBuf[36], (uint8_t*)&fTmp, sizeof(fTmp) );		// RAAC
			fTmp = 0;
			memcpy( &ucTxBuf[40], (uint8_t*)&fTmp, sizeof(fTmp) );		// RSAC
			fTmp = FuelReg.RARC;
			memcpy( &ucTxBuf[44], (uint8_t*)&fTmp, sizeof(fTmp) );		// RARC
			fTmp = 0;
			memcpy( &ucTxBuf[48], (uint8_t*)&fTmp, sizeof(fTmp) );		// RSRC
			fTmp = FuelReg.FullCapacity;
			memcpy( &ucTxBuf[52], (uint8_t*)&fTmp, sizeof(fTmp) );		// Full Capacity [mAh]
			fTmp = 0;
			memcpy( &ucTxBuf[56], (uint8_t*)&fTmp, sizeof(fTmp) );		// FullRelative
			fTmp = 0;
			memcpy( &ucTxBuf[60], (uint8_t*)&fTmp, sizeof(fTmp) );		// ActiveEmpty
			fTmp = 0;
			memcpy( &ucTxBuf[64], (uint8_t*)&fTmp, sizeof(fTmp) );		// ActiveEmptyRelative
			fTmp = 0;
			memcpy( &ucTxBuf[68], (uint8_t*)&fTmp, sizeof(fTmp) );		// StandbyEmpty
			fTmp = 0;
			memcpy( &ucTxBuf[72], (uint8_t*)&fTmp, sizeof(fTmp) );		// StandbyEmptyRelative
			fTmp = 0;
			memcpy( &ucTxBuf[76], (uint8_t*)&fTmp, sizeof(fTmp) );		// AgeScalar
			fTmp = 0;
			memcpy( &ucTxBuf[80], (uint8_t*)&fTmp, sizeof(fTmp) );		// AccuCurrent

			send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, &ucTxBuf[0], sizeof(MsgBase) + MsgBase.m_cmdDataSize );

		//	if ( MsgBase.m_cmdParam == 6 ) { DevReg.PowerDownTimeout = DEF_PWDN_NO_TIMEOUT; }
		//	if ( MsgBase.m_cmdParam == 7 ) { DevReg.PowerDownTimeout = DEF_PWDN_15MIN_TIMEOUT; }
		//	if ( MsgBase.m_cmdParam == 8 ) { DevReg.PowerDownTimeout = DEF_PWDN_5MIN_TIMEOUT; }
		//	if ( MsgBase.m_cmdParam == 9 ) { cpu_delay_ms(100); DevReg.PowerDownTimeout = 0; }

		}
		break;


		/* **************************************************************** */
		/* ************************** DUMMY CMD *************************** */
		/* **************************************************************** */

		case cmdDummy:
		break;

		/* **************************************************************** */
		/* **************************** ERROR ***************************** */
		/* **************************************************************** */
		case cmdError:
		{
			DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;
		}
		break;

		default:
		{
			DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;
			add_error(ErrCnt,errNotCmd);
		}
		break;

		/* **************************************************************** */
		/* ***************************** END ****************************** */
		/* **************************************************************** */
	}

	if ( ErrCnt == 0 )
	{
		ESP_LOGI(tag, " mVibe: command receive OK\n" );
	}
	else
	{
		ESP_LOGE(tag, " mVibe: command receive error: cntErr = %d, numErr = 0x%02x\n", ErrCnt, ErrBuffer[0] );
		send_error_buffer( sockIdx );
	}
}

//
static void send_error_buffer( int sockIdx )
{
	uint32_t	ulLen = ErrCnt * sizeof(uint32_t);
	msg_base_t	MsgBase;

	MsgBase.m_key = CMD_KEY;
	MsgBase.m_cmd = cmdError;
	MsgBase.m_cmdParam = 0;
	MsgBase.m_cmdDataSize = ulLen;
	memcpy( &ucTxBuf[0], (uint8_t*)&MsgBase, sizeof(MsgBase) );

	for( int i = 0; i < ErrCnt; i++ )
	{
		uint32_t ulTmp = ErrBuffer[i];
		memcpy( &ucTxBuf[(4+i)*sizeof(uint32_t)], (uint8_t*)&ulTmp, sizeof(uint32_t) );
	}

	send_tcp( mySockReg[ sockIdx ].acceptedSocketHdl, &ucTxBuf[0], sizeof(MsgBase) + ulLen );
	ErrCnt = 0;
	DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;
	DevState.MeasurementRemoteState = REMOTE_MEASURE_IDLE;
	Flags_Wraper.err_wraper = 0;

	vTaskDelay( 1 );
	measure_flag = 0;
	vTaskDelay( 5 );
	led( 0, 0, 255, 0 );
	vTaskDelay( 5 );

	return;
}

//
static int send_tcp( SOCKET sock, uint8_t *buf, uint32_t len )
{
	int16_t		retval 		= ESP_OK;

	uint8_t		*tmpBuf		= buf;
	uint32_t	tmpLen		= len;

	uint32_t	idx 		= WSOCK_0_IDX;

	while ( tmpLen > TCP_PACKET_MAX_LEN )
	{
		if ( mySockReg[ idx ].acceptedSocketHdl < 0 )
		{
			retval = ESP_FAIL;
		}
		else
		{
			retval = send( sock, tmpBuf, TCP_PACKET_MAX_LEN, 0 );
		}

		if ( retval != ESP_FAIL )
		{
			tmpBuf += TCP_PACKET_MAX_LEN;
			tmpLen -= TCP_PACKET_MAX_LEN;
		}
		else
		{
			tmpLen = 0;
			break;
		}
	}

	if ( tmpLen > 0 )
	{
		if ( mySockReg[ idx ].acceptedSocketHdl < 0 )
		{
			retval = ESP_FAIL;
		}
		else
		{
			retval = send( sock, tmpBuf, tmpLen, 0 );
		}
	}

	if ( retval == ESP_FAIL )
	{
		close( mySockReg[ idx ].acceptedSocketHdl );
		mySockReg[ idx ].acceptedSocketHdl = -1;

		DevState.MeasurementRemoteState = REMOTE_MEASURE_STOPPED;//bylo 1
		vTaskDelay(10);
	}

	return retval;
}

//
void task_dev_udp_server_main( void *udp )
{

	int status;
	unsigned int recvCnt 	= 0;
	unsigned int saddr_len 	= sizeof(struct sockaddr) ;
	unsigned int flag_on 	= 1;

	esp_task_wdt_delete( NULL );

	while( 1 )
	{
		reset_struct_mySockReg( WSOCK_2_IDX );

		xEventGroupWaitBits( wifi_sta_event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY );

		mySockReg[ WSOCK_2_IDX ].listenSocketHdl = socket(AF_INET,SOCK_DGRAM,IPPROTO_IP);
		if( mySockReg[ WSOCK_2_IDX ].listenSocketHdl == -1 )
		{
			ESP_LOGE( tag, "udp_socket: %d, errno: %s", mySockReg[ WSOCK_2_IDX ].listenSocketHdl, strerror(errno) );
			continue;
		}

		status = setsockopt( mySockReg[ WSOCK_2_IDX ].listenSocketHdl, SOL_SOCKET, SO_BROADCAST, &flag_on, sizeof(flag_on) );
		if ( status != 0 )
		{
			ESP_LOGE( tag, "UDP setsockopt: %d, errno: %s", status, strerror(errno) );
			close(mySockReg[ WSOCK_2_IDX ].listenSocketHdl);
			continue;
		}

		status = bind( mySockReg[ WSOCK_2_IDX ].listenSocketHdl, (struct sockaddr *)&mySockReg[ WSOCK_2_IDX ].socketAddr, sizeof(struct sockaddr_in) );
		if ( status != 0 )
		{
			ESP_LOGE( tag, "UDP bind: %d, errno: %s", status, strerror(errno) );
			close(mySockReg[ WSOCK_2_IDX ].listenSocketHdl);
			continue;
		}

		ESP_LOGI( tag, "WSOCK_2_IDX: new udp_listenSocketHdl = %d", mySockReg[ WSOCK_2_IDX ].listenSocketHdl );

		do
		{
			mySockReg[ WSOCK_2_IDX ].recvLen = recvfrom( mySockReg[ WSOCK_2_IDX ].listenSocketHdl, gau8SocketBufferUDP, sizeof(gau8SocketBufferUDP), 0, (struct sockaddr *)&mySockReg[ WSOCK_2_IDX ].socketAddr, &saddr_len );
			ESP_LOGI(tag, "WSOCK_2_IDX (UDP): recvCnt: %d, recvLen: %d \n", recvCnt, mySockReg[ WSOCK_2_IDX ].recvLen );

			if( mySockReg[ WSOCK_2_IDX ].recvLen > 0 )
			{
				recvCnt++;
				dev_udp_server_mvibe( WSOCK_2_IDX );
			}
			else if ( mySockReg[ WSOCK_2_IDX ].recvLen == -1 )
			{
				mySockReg[ WSOCK_2_IDX ].listenSocketHdl = -1;
				close( mySockReg[ WSOCK_2_IDX ].listenSocketHdl );
			}

		} while ( mySockReg[ WSOCK_2_IDX ].listenSocketHdl != -1 );
	}
	vTaskDelete(NULL);
}

//
void task_dev_tcp_server_main( void *tcp_main )
{
	int status;
	unsigned int saddr_len = sizeof(struct sockaddr);
	unsigned int recvCnt = 0;

	esp_task_wdt_delete( NULL );

	while( 1 )
	{

		vTaskDelay(100);
		reset_struct_mySockReg( WSOCK_0_IDX );

		xEventGroupWaitBits( wifi_sta_event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY );

		if(recvCnt > 10)
		{
			esp_restart();
		}

		mySockReg[ WSOCK_0_IDX ].listenSocketHdl = socket( AF_INET, WIFI_SOCKET_0_TYPE, 0 );
		if(mySockReg[ WSOCK_0_IDX ].listenSocketHdl < 0)
		{
			recvCnt++;
			ESP_LOGE( tag, "TCP socket: %d, errno: %s", mySockReg[ WSOCK_0_IDX].listenSocketHdl, strerror(errno) );
			continue;
		}

		status = bind( mySockReg[ WSOCK_0_IDX ].listenSocketHdl, (struct sockaddr *)&mySockReg[ WSOCK_0_IDX ].socketAddr, sizeof(struct sockaddr_in) );
		if ( status != 0 )
		{
			recvCnt++;
			ESP_LOGE( tag, "TCP bind: %d, errno: %s", status, strerror(errno) );
			close( mySockReg[ WSOCK_0_IDX ].listenSocketHdl );
			continue;
		}

		status = listen(mySockReg[ WSOCK_0_IDX ].listenSocketHdl, 4);
		if ( status != 0 )
		{
			recvCnt++;
			ESP_LOGE( tag, "listen: %d, errno: %s", status, strerror(errno) );
			close(mySockReg[ WSOCK_0_IDX ].listenSocketHdl);
			continue;
		}

		ESP_LOGI( tag, "WSOCK_0_IDX: new tcp_listenSocketHdl = %d", mySockReg[ WSOCK_0_IDX ].listenSocketHdl );

		int prev_acceptedSocketHdl = -1;

		do
		{
			recvCnt = 0;
			mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl = accept( mySockReg[ WSOCK_0_IDX ].listenSocketHdl, (struct sockaddr *)&mySockReg[ WSOCK_0_IDX ].socketAddr, &saddr_len );
			ESP_LOGI( tag, "WSOCK_0_IDX: new tcp_acceptedSocketHdl = %d", mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl );
			mySockReg[ WSOCK_0_IDX ].FlagCheckSocketTcp = true;



			if ( prev_acceptedSocketHdl != -1 )
				close( prev_acceptedSocketHdl );
			prev_acceptedSocketHdl = mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl;



		} while ( mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl != -1 );

		mySockReg[ WSOCK_0_IDX ].listenSocketHdl = -1;
	}
	vTaskDelete(NULL);
}

//
void task_dev_tcp_server_recv( void *tcp_rcv )
{
	unsigned int recvCnt = 0;

	esp_task_wdt_delete( NULL );

	while( 1 )
	{
		if ( mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl != -1 )
		{
			mySockReg[ WSOCK_0_IDX ].recvLen  =	recv( mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl, gau8SocketBuffer, TCPIP_BUFF_SIZE, 0 );

			ESP_LOGI(tag, "WSOCK_0_IDX (TCP): recvCnt: %d, recvLen: %d \n", recvCnt, mySockReg[ WSOCK_0_IDX ].recvLen );

			if( mySockReg[ WSOCK_0_IDX ].recvLen >= 0 )
			{
				recvCnt++;
				dev_tcp_server_mvibe( WSOCK_0_IDX );
			}
			else if( mySockReg[ WSOCK_0_IDX ].recvLen == -1 )
			{
				mySockReg[ WSOCK_0_IDX ].acceptedSocketHdl = -1;
			}
		}
	}
	vTaskDelete(NULL);
}

//
void task_dev_tcp_server_measurement( void *tcp_meas )
{

	esp_task_wdt_delete( NULL );

	while( 1 )
	{
		dev_measurement_remote_mvibe( WSOCK_0_IDX );
	}
	vTaskDelete(NULL);
}

/* ---------------------------------------------------------------------------------------- */

//
static void deep_sleep_gpio_set( void )
{
    fram_write_DevReg( &DevReg ,sizeof(DevReg));
    vTaskDelay(10);

	gpio_num_t gpio_num = GPIO_NUM_12;		// FRAM_MISO
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_13;					// FRAM_MOSI
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_14;					// FRAM_SCLK
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_15;					// FRAM_CS
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_25;					// SCL
	rtc_gpio_init( gpio_num );
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis( gpio_num );
	rtc_gpio_pullup_dis( gpio_num );

	gpio_num = GPIO_NUM_26;					// SDA
	rtc_gpio_init( gpio_num );
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis( gpio_num );
	rtc_gpio_pullup_dis( gpio_num );

	gpio_num = GPIO_NUM_27;					// LSM_CS
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);	// ???

	gpio_num = GPIO_NUM_0;					// I2S_DOUT / IO0
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_en(gpio_num);	// strap pin

	gpio_num = GPIO_NUM_2;					// I2S_LRCK
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_4;					// I2S_BCK
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_33;					// LSM_DEN
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);

	gpio_num = GPIO_NUM_35;					// LSM_INT
	rtc_gpio_init(gpio_num);
	rtc_gpio_set_direction(gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);


	gpio_num = GPIO_NUM_5;					// EN_LED (active high)
	rtc_gpio_pulldown_en(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);
	gpio_pad_select_gpio(gpio_num);
	gpio_set_direction(gpio_num, GPIO_MODE_DEF_OUTPUT);
	gpio_set_level(gpio_num, 0);

	gpio_num = GPIO_NUM_16;					// EN_AUX (active high)
	rtc_gpio_pulldown_en(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);
	gpio_pad_select_gpio(gpio_num);
	gpio_set_direction(gpio_num, GPIO_MODE_DEF_OUTPUT);
	gpio_set_level(gpio_num, 0);

	gpio_num = GPIO_NUM_17;					// LED_I2S
	rtc_gpio_pulldown_dis(gpio_num);
	rtc_gpio_pullup_dis(gpio_num);
	gpio_pad_select_gpio(gpio_num);
	gpio_set_direction(gpio_num, GPIO_MODE_DEF_INPUT);

	ESP_LOGI( tag, "esp_deep_sleep_start" );
	esp_deep_sleep_start();
}

//
static void calibrate_touch_pad( touch_pad_t pad, int * threshold )
{
    touch_pad_config(pad, 0);

    vTaskDelay(100);

    int avg = 0;
    const size_t calibration_count = 128;
    for (int i = 0; i < calibration_count; ++i)
    {
        uint16_t val;
        touch_pad_read(pad, &val);
        avg += val;
    }
    avg /= calibration_count;
    const int min_reading = 500;
    if (avg < min_reading)
    {
        ESP_LOGI(tag,"Touch pad #%d average reading is too low: %d (expecting at least %d). "
               "Not using for deep sleep wakeup.\n", pad, avg, min_reading);
        touch_pad_config(pad, 0);
    }
    else
    {
        *threshold = (int)( 0.975 * avg );
        ESP_LOGI(tag,"Touch pad #%d average: %d, wakeup threshold set to %d.\n", pad, avg, *threshold);
        touch_pad_config(pad, *threshold);
    }
}

//
static void mvibe_common_init( void )
{
//    esp_sleep_wakeup_cause_t cause = esp_sleep_get_wakeup_cause();
//
//    switch (cause)
//    {
//		case ESP_SLEEP_WAKEUP_TOUCHPAD:
//			ESP_LOGI(tag,"TOUCHPAD wakeup!!!\n");
//			break;
//		case ESP_SLEEP_WAKEUP_ULP:
//			ESP_LOGI(tag,"ULP program wakeup!!!\n");
//			break;
//		default:
//			ESP_LOGI(tag,"UNDEFINED wakeup!!!\n");
//			break;
//    }

	gpio_num_t gpio_num = GPIO_NUM_16;		// EN_AUX
	rtc_gpio_pulldown_dis( gpio_num );
	rtc_gpio_pullup_en( gpio_num );
	gpio_set_level( gpio_num, 1 );
	gpio_pad_select_gpio( gpio_num );
	gpio_set_direction( gpio_num, GPIO_MODE_DEF_OUTPUT );

	rtc_gpio_hold_dis( GPIO_NUM_5 );
	rtc_gpio_hold_dis( GPIO_NUM_16 );
	rtc_gpio_hold_dis( GPIO_NUM_17 );

	rtc_gpio_hold_dis( GPIO_NUM_0 );		// I2S_DOUT
	rtc_gpio_hold_dis( GPIO_NUM_2 );		// I2S_LRCK
	rtc_gpio_hold_dis( GPIO_NUM_4 );		// I2S_BCK
	rtc_gpio_hold_dis( GPIO_NUM_12 );		// FRAM_MISO
	rtc_gpio_hold_dis( GPIO_NUM_13 );		// FRAM_MOSI
	rtc_gpio_hold_dis( GPIO_NUM_14 );		// FRAM_SCLK
	rtc_gpio_hold_dis( GPIO_NUM_15 );		// FRAM_CS
	rtc_gpio_hold_dis( GPIO_NUM_25 );		// SCL
	rtc_gpio_hold_dis( GPIO_NUM_26 );		// SDA
	rtc_gpio_hold_dis( GPIO_NUM_27 );		// LSM_CS
	rtc_gpio_hold_dis( GPIO_NUM_33 );		// LSM_DEN
	rtc_gpio_hold_dis( GPIO_NUM_35 );		// LSM_INT

	rtc_gpio_deinit( GPIO_NUM_0 );
	rtc_gpio_deinit( GPIO_NUM_2 );
	rtc_gpio_deinit( GPIO_NUM_4 );
	rtc_gpio_deinit( GPIO_NUM_12 );
	rtc_gpio_deinit( GPIO_NUM_13 );
	rtc_gpio_deinit( GPIO_NUM_14 );
	rtc_gpio_deinit( GPIO_NUM_15 );
	rtc_gpio_deinit( GPIO_NUM_25 );
	rtc_gpio_deinit( GPIO_NUM_26 );
	rtc_gpio_deinit( GPIO_NUM_27 );
	rtc_gpio_deinit( GPIO_NUM_33 );
	rtc_gpio_deinit( GPIO_NUM_35 );

    ws2812_init( WS2812_PIN );
    vTaskDelay(10);
    led( 255,215,0, 0 );

	fram_init();
	fram_read_DevReg( &DevReg, sizeof(DevReg) );

    if(ADC_SENS_FLAG == 1)
    {
    	sensor_sens_init();
    	fram_write_SensorSens( &SensorSens, sizeof(SensorSens) );
    	ESP_LOGI(tag,"\n\n WRITING SENS SETTINGS TO FRAM \n\n");
    }
    else
    {
    	fram_read_SensorSens( &SensorSens, sizeof(SensorSens) );
    	card_param.DefSerialNo[ 5 ] = SensorSens.serial_no;
    	for( int i = 0; i < NOSENSORS; i ++)
    		card_param.DefSensorSens[ i ] = SensorSens.Sensor_Sens[ i ];

    	ESP_LOGI(tag,"\n\n READING SENS SETTINGS FROM FRAM \n\n");
    }


    touch_pad_init();
    // Set measuring time and sleep time
    // In this case, measurement will sustain 0xffff / 8Mhz = 8.19ms
    // Meanwhile, sleep time between two measurement will be 0x1000 / 150Khz = 27.3 ms
    touch_pad_set_meas_time( SensorSens.button_time, SensorSens.button_sleep );
    // Set reference voltage for charging/discharging
    // In this case, the high reference voltage will be 2.4V - 1.5V = 0.9V
    // The low reference voltage will be 0.8V, so that the procedure of charging
    // and discharging would be very fast.
    touch_pad_set_voltage( TOUCH_HVOLT_2V4, TOUCH_LVOLT_0V8, TOUCH_HVOLT_ATTEN_1V5 );

    if(DevReg.Fram_version != DEV_REG_REFRESH)
    {
    	// Calibrate touch pad threshold
    	calibrate_touch_pad( TOUCH_PAD_NUM9, &DevReg.tpThreshold );
    }
    else
    {
    	touch_pad_config( TOUCH_PAD_NUM9, DevReg.tpThreshold );
    }

    esp_sleep_enable_touchpad_wakeup();
    ESP_LOGI(tag,"DEEPSLEEP wakeup source: touchpad\n");


	if(DevReg.Fram_version != DEV_REG_REFRESH)
	{
		ESP_LOGI(tag,"\n\n DevReg init \n\n");
		dev_reg_init();
		fram_write_DevReg( &DevReg, sizeof(DevReg) );
	}

    udp_response_msg_init();

    i2c_config();

    max17055_init( DevReg );

    struct_mySockReg_init();

    sleep_pcm();

}

//
static void task_mvibe_common( void *param )
{
	static int longPressCnt = 0;
	uint16_t touch_value;

	esp_task_wdt_delete( NULL );

	networkInit();

	while(1)
	{
		int nofFlashes = 2;

		if (otaUpdateInProgress())
		{
			nofFlashes += 2; // results in 3 (not connected) or 4 (connected) flashes
		}

		max17055_read( &FuelReg );

//		printf( "Read MAX17055: %dmV, %dmA, FC %dmAh, RAAC %dmAh, RARC %d%%, T %dC \n",
//				FuelReg.BatteryVoltage,FuelReg.BatteryCurrent, FuelReg.FullCapacity, FuelReg.RAAC, FuelReg.RARC, FuelReg.BatteryTemperature  );
		if( FuelReg.RARC < 10 )
		{
			led( 255,165,0, 1 );
			vTaskDelay(100);
			led( 0,0,0, 1 );
			vTaskDelay(100);
			led( 255,165,0, 1 );
			vTaskDelay(100);
			led( 0,0,0, 1 );
			vTaskDelay(100);
			led( 255,165,0, 1 );
			vTaskDelay(200);

			deep_sleep_gpio_set();
		}
		if ( !measure_flag )
		{
		touch_pad_read( TOUCH_PAD_NUM9, &touch_value);
		ESP_LOGI(tag, "touch_value: %d %d ==> %4.1f \n", touch_value, DevReg.tpThreshold, 100.0*touch_value/DevReg.tpThreshold  );

		if( touch_value < DevReg.tpThreshold )
		{
			if( longPressCnt > 5 )
			{
				led( 136,20,255, 1 );
				vTaskDelay(200);
				deep_sleep_gpio_set();
			}

			led( 68,10,127, 1 );
			longPressCnt++;
			ESP_LOGI(tag, "longPressCnt: %d \n", longPressCnt );
		}
		else
		{
			led( 0,0,0, 2 );
			longPressCnt = 0;
		}
	}
		vTaskDelay(50);
	}

	vTaskDelete(NULL);
}

//
void restore_deafult()
{
	int cnt = 0;
	uint16_t touch_value;

	led( 255,215,0, 0 );

	while(cnt < 3)
	{
		touch_pad_read( TOUCH_PAD_NUM9, &touch_value );
		vTaskDelay(100);
		if( touch_value < DevReg.tpThreshold )
		{
			vTaskDelay(300);
			touch_pad_read( TOUCH_PAD_NUM9, &touch_value );
			if( touch_value < DevReg.tpThreshold )
			{
				dev_reg_init();
				fram_write_DevReg( &DevReg, sizeof(DevReg) );
				break;
			}
		}
		cnt++;
	}

	led( 255,0,0, 0 );
}

void disable_log()
{
	 esp_log_level_set( "MAIN", ESP_LOG_NONE );
	 esp_log_level_set( "wifi", ESP_LOG_NONE );
	 esp_log_level_set( "event_handler", ESP_LOG_NONE);
	 esp_log_level_set( "MAX", ESP_LOG_NONE );
	 esp_log_level_set( "SPI", ESP_LOG_NONE );
	 esp_log_level_set( "network", ESP_LOG_NONE );
	 esp_log_level_set( "I2S", ESP_LOG_NONE );
	 esp_log_level_set( "event", ESP_LOG_NONE );
	 esp_log_level_set( "Analog", ESP_LOG_NONE );

}

/* ---------------------------------------------------------------------------------------- */

//
void app_main(void)
{
	//disable_log();

    mvibe_common_init();
    vTaskDelay(20);

	restore_deafult();
	vTaskDelay(10);

	wifi_init_sta();
	vTaskDelay(10);

	xTaskCreate( &task_mvibe_common, "mvibe_sys", 16348, NULL, 5, NULL );
	vTaskDelay(10);

	xTaskCreate( &task_dev_udp_server_main, "udp_svr", 16348, NULL, 3, NULL );
	vTaskDelay(10);

	xTaskCreate( &task_dev_tcp_server_main, "tcp_svr", 16348, NULL, 2, NULL );
	vTaskDelay(10);

	xTaskCreate( &task_dev_tcp_server_recv, "tcp_svr_rcv", 16348, NULL, 1, NULL );
	vTaskDelay(10);

	xTaskCreate( &task_dev_tcp_server_measurement, "tcp_svr_meas", 16348, NULL, 1, NULL );
	vTaskDelay(10);

}


