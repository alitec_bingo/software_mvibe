/*
 * ws2812B.h
 *
 *  Created on: 4 gru 2017
 *      Author: admin
 */

#ifndef MAIN_WS2812B_H_
#define MAIN_WS2812B_H_

#include <stdint.h>
#include <stdbool.h>

bool led_flag;

typedef union
{
	struct __attribute__ ((packed))
	{
		uint8_t r, g, b;
	};
	uint32_t num;
} rgbVal;


inline rgbVal makeRGBVal(uint8_t r, uint8_t g, uint8_t b)
{
	rgbVal v;

	v.r = r;
	v.g = g;
	v.b = b;
	return v;
}

void ws2812_init(int gpioNum);
void ws2812_setColors(unsigned int length, rgbVal *array);

#endif /* MAIN_WS2812B_H_ */
