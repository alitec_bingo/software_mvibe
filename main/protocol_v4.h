/*
 * protocol_v4.h
 *
 * Created: 2011-09-22 15:37:21
 *  Author: mmakow
 */

#ifndef PROTOCOL_V4_H_
#define PROTOCOL_V4_H_

#include <stdint.h>

// *************************************************************************** //
// ******************* COMUNICATION PROTOCOL OVER ETHERNET ******************* //
// *************************************************************************** //

typedef struct _msg_base_t
{
	uint32_t		m_key;				// protocol password
	uint32_t		m_cmd;				// command
	uint32_t		m_cmdParam;			// command parameter
	uint32_t		m_cmdDataSize;		// data size of data

} msg_base_t;

#define CMD_KEY						0xE93CD74B		// NEW VERSION !!! dec: 3 913 078 603

#define cmdDummy					0xFF

#define cmdCloseConnection			0x00

#define cmdCardParametersRead		0x01
#define cmdCardParameters			0x81

#define cmdConfigurationSet	  		0x02	// host->dev, dev->host
#define cmdConfigurationDone 		0x82	// dev->host
#define cmdConfigurationGive  		0x12	// host->dev
	#define paramUseAllParam			0x00	// use all parameters from command
	#define paramUseAllParamAndSave		0x01	// use all parameters from command and save configuration in NVRAM
	#define paramUseMainParam			0x02	// use all parameters expect for ChSensorSens, ChCalGain, ChCalOffset
	#define paramUseMainParamAndSave	0x03	// use all parameters expect for ChSensorSens, ChCalGain, ChCalOffset and save configuration in NVRAM

#define cmdScanSensorsStart			0x03
#define cmdScanSensorsReady			0x73
#define cmdMeasuredData				0x83
#define cmdScanSensorsStop			0x04
#define cmdScanSensorsStopped		0x84
	#define paramSingleMode				0x01
	#define paramContinuousMode			0x02
	#define paramLCDMode				0x04

	#define paramAnalogDigit			0x00
	#define paramCounterData			0x10

	#define MaskParamMode				0x0F
	#define MaskParamData				0xF0

#define cmdTemperatureRead			0x06
#define cmdTemperatureData			0x86
#define cmdTemperatureReadStop	  	0x07
#define cmdTemperatureReadStopped 	0x87

#define cmdCalibrationStart			0x08
#define cmdCalibrationDone			0x88

#define cmdError					0x0B

#define cmdFirmwareUpgrade			0x0C
#define cmdFirmwareUpgradeDone		0x8C

	#define paramFirmwareMCU			0x65	// MCU firmware
	#define paramFirmwareRSI			0x66	// RedPine WiFi firmware

#define cmdBatteryStatusRead		0x0D
#define cmdBatteryStatusDone		0x8D

#define cmdLedSet					0x0E
#define cmdLedSetDone				0x8E

#define cmdWiFiSettingsSet			0x0F
#define cmdWiFiSettingsDone			0x8F

#define cmdRFMStatusRead			0x23	// CHECK!!!!!
#define cmdRFMStatusDone			0xA3

#define cmdCalibrationSet			0x28
#define cmdCalibrationSetDone		0xA8

#define cmdLedSet					0x0E
#define cmdLedSetDone				0x8E

// ******************* Error Table ******************* //

#define MAX_ERR_CNT				31
#define errRX					0xA0	// received error
#define errFsLow				0xA1	// FS < MIN_FS
#define errFsHigh				0xA2	// FS > MAX_FS
#define errFsApprox				0xA3	// FS is approximate
#define errNoConfig				0xA4	// no Configuration Settings
#define errWrongChannel			0xA5	// channel no exist
#define errNOS					0xA6	// number of samples > MAX_NOS
#define errNOCH					0xA7	// number of channels > MAX_NOCH
#define errSensorType			0xA8	//
#define errNotCmd				0xA9	// received message is not command
#define errRxOverflow			0xAA	// RX buffer overflow
#define errCardType				0xAB	// wrong card type
#define errSerialNo				0xAC	// wrong serial number
#define errTrigger				0xAD	// wrong trigger value
#define errChUnit				0xAE	// wrong unit
#define	errCalibration 			0xAF
#define errMissmatchFilter		0xB0
#define errPrecision			0xB1
#define errFilterType			0xB2
#define errBridgeType			0xB3
#define errInputType			0xB4
#define errChActiveTens			0xB5
#define errRange				0xB6
#define errVoltage				0xB7
#define errTensConst			0xB8
#define errTensForce			0xB9
#define errTensBase				0xBA
#define errTensDeflection		0xBB
#define errVoltageType			0xBC
#define errMissmatchVoltageType 0xBD
#define errSensorSens			0xBE
#define errChGain				0xBF
#define errSensIntExtSw			0xC0

#define errFwUnknowDevice		0x80
#define errFwTooManyFiles		0x81
#define errFwBufferOverflow		0x82
#define errFwFileCrc			0x83

#define errMeasurementMode		0x90
#define errPackOfSamples		0x91		// number of samples in pack
#define errMeasurementPeriod	0x92
#define errMeasurementDataRead	0x93
#define errDevUnderRemoteMeasure 0x94
#define errNumberOfMeasurements 0x95
#define errDataBufferOv			0x96
#define errMeasurement			0x97

#define add_error(cnt,err_no)	{ ErrBuffer[cnt] = err_no; if (cnt < MAX_ERR_CNT) {cnt++;} }

// *************************************************************************** //

typedef enum
{
	RX_IDLE_STATE = 0,
	RX_CMD_PART_RECEIVED = 1,

} rx_cmd_state;

typedef enum
{
	S_PASS = 0,
	S_NOPASS = 1

} sock_pass;

typedef struct _rx_state_t
{
	rx_cmd_state	cmd_state;
//	sock_pass		sock_first[8];

} rx_state_t;


typedef struct _card_param_t
{
	uint32_t 	DefSerialNo[6];				// Default Serial Number
	uint32_t 	DefCardType;				// Default Card Type
	uint32_t 	MaxNumberOfSamples;			// Max number of samples
	uint32_t 	MaxNumberOfChannels;		// Max number of channels

	uint32_t 	NoVChT;
	uint32_t 	DefChannelType[NOVCHT][MAX_NOCH];

	uint32_t 	NoVFS;
	uint32_t 	DefValidFS[NOVFS];

	uint32_t 	NoVR;
	float 		DefValidRange[NOVR*2][MAX_NOCH];

	uint32_t 	NoSensor;
#if NOSENSORS != 0
	float 		DefSensorSens[NOSENSORS];
	float 		DefSensorFreqRange[NOSENSORS*2];
#endif

	uint32_t 	SensorIntExtSw;

	float 		FabChGainInt[NOVR][MAX_NOCH];
	float 		FabChOffsetInt[NOVR][MAX_NOCH];
	float 		FabChGainExt[NOVR][MAX_NOCH];
	float 		FabChOffsetExt[NOVR][MAX_NOCH];

} card_param_t;


typedef struct _card_param_udp_t
{
	uint32_t	DefCardType;				// Default Card Type
	uint32_t 	MaxNumberOfSamples;			// Max number of samples
	uint32_t 	MaxNumberOfChannels;		// Max number of channels

	uint32_t 	NoVChT;
	uint32_t 	DefChannelType[NOVCHT][MAX_NOCH];

	uint32_t 	NoVFS;
	uint32_t 	DefValidFS[NOVFS];

	uint32_t 	NoVR;
	float 		DefValidRange[NOVR*2][MAX_NOCH];

	uint32_t 	NoSensor;
#if NOSENSORS != 0
	float 		DefSensorSens[NOSENSORS];
	float 		DefSensorFreqRange[NOSENSORS*2];
#endif

	uint32_t 	SensorIntExtSw;

} card_param_udp_t;


typedef struct _message_udp_t
{
	msg_base_t			header;
	uint32_t			serialNo[6];
	card_param_udp_t	cp;

} message_udp_t;











// ******************* BBOX IEPE ******************* //

// device master/slave type
#define DEV_SLAVE			0x00
#define DEV_MASTER			0x01
#define MAX_DEV_TRIG		DEV_MASTER

// input type
#define DEV_IN_EMPTY		0xFFFFFFFF
#define DEV_IN_DIG			0x00
#define DEV_IN_AC_ISRC20	0x01
#define DEV_IN_AC			0x02
#define DEV_IN_DC			0x03
#define DEV_IN_AC_ISRC12	0x04
#define DEV_IN_DC_ISRC12	0x05
#define DEV_IN_DC_ISRC20	0x06
#define DEV_IN_DC_PT100		0x07

#define DEV_IN_VZERO		0x0A
#define DEV_IN_VREF			0x0B
#define MAX_DEV_INPUT		DEV_IN_VREF

// channel type
//#define CH_OFF				0x00
//#define CH_ANA				0x01
//#define CH_DIG				0x02
//#define CH_ANA_DIG			0x03
//#define MAX_CH_TYPE			CH_ANA_DIG

// sample frequencies
#define FS_131072			131072
#define FS_98304			98304
#define FS_65536			65536
#define FS_32768			32768
#define FS_16384			16384
#define FS_8192				8192
#define FS_4096				4096
#define FS_2048				2048
#define FS_1024				1024
#define FS_512				512
#define FS_256				256
#define FS_128				128
#define FS_64				64
#define FS_32				32
#define FS_16				16
#define FS_8				8
#define FS_4				4
#define FS_2				2
#define FS_1				1

// channel ranges
#define R_25				25.0
#define R_10				10.0
#define R_5					5.0
#define R_2					2.0
#define R_1					1.0
#define R_05				0.5
#define R_02				0.2
#define R_01				0.1

#define R_15				15.0
#define R_12				12.0
#define R_6					6.0
#define R_3					3.0
#define R_2_5				2.5
#define R_1_25				1.25
#define R_1_2				1.2
#define R_0_6				0.6

#define R_20				20.0
#define R_0_0				0.0


#define SENSOR_NA			0
#define SENSOR_INTERNAL		1
#define SENSOR_EXTERNAL		2

#define SENSOR_ONLY_INTERNAL -1

// channel units
#define ADS_UNIT_RAW		0x00
#define ADS_UNIT_uV			0x01
#define ADS_UNIT_mV			0x02
#define ADS_UNIT_V			0x03
#define ADS_UNIT_mg			0x05
#define ADS_UNIT_g			0x06
#define ADS_UNIT_mms2		0x07
#define ADS_UNIT_ms2		0x08
#define ADS_UNIT_mms		0x09
#define ADS_UNIT_ms			0x0a
#define ADS_UNIT_um			0x0b
#define ADS_UNIT_mm			0x0c
#define ADS_UNIT_Celsius	0x0d
#define ADS_UNIT_rpm		0x0e		// revolutions per minute
#define ADS_UNIT_rps		0x0f		// revolutions per second Hz
#define MAX_UNIT			ADS_UNIT_ms2


// ******************* SG ******************* //

#define NORMAL_PREC			0
#define HIGH_PREC			1

#define VOLTAGE_AC			0
#define HIGH_PREC			1

#define FILTER_OFF			0
#define FILTER_NORMAL		1
#define FILTER_FAST			2

#define FULL_BRIDGE			0
#define HALF_BRIDGE			1

#define INPUT_BIPOLAR		0
#define INPUT_UNIPOLAR		1

#define INPUT_DC			0
#define INPUT_AC			1

// channel units
#define UNIT_mV_V			0
#define UNIT_um_m			1
#define UNIT_uD				2
#define UNIT_N				3
#define UNIT_kG				4
#define UNIT_RAW			5


// ******************* State Machine ******************* //

typedef enum
{
	MODE_IDLE,
	MODE_MONITOR,
	MODE_MEASUREMENT_CARD

} device_mode_state_t;

typedef enum
{
	MEASURE_IDLE = 0,
	MEASURE_STOPPED = 1,
	MEASURE_START = 2,
	MEASURE_RESTART = 3,
	MEASURE_IN_PROGRESS = 4,
	MEASURE_FINISHED = 5,
	MEASURE_UNFINISHED = 6

} measure_state_t;

typedef enum
{
	REMOTE_MEASURE_IDLE	= 0,
	REMOTE_MEASURE_WAIT_TO_STOP = 1,
	REMOTE_MEASURE_STOPPED = 2,
	REMOTE_MEASURE_START = 3,
	REMOTE_MEASURE_IN_PROGRESS = 4

} remote_measure_state_t;

typedef enum
{
	PARAM_SINGLE_MODE = paramSingleMode,
	PARAM_CONTINUOUS_MODE = paramContinuousMode,
	PARAM_LCD_MODE = paramLCDMode

} remote_measure_mode_t;

typedef enum
{
	PARAM_ANALOG_DIGITAL = paramAnalogDigit,
	PARAM_COUNTER_DATA = paramCounterData

} remote_measure_param_t;

typedef enum
{
	CONFIG_SET,
	CONFIG_NOSET

} config_state_t;



#define MEASURE_ALL_PAGES	0x00
#define MEASURE_SINGLE_PAGE	0x01


// ****************************************************** //
// **************** CONSTANTS BBOX IEPE ***************** //
// ****************************************************** //

#define VREF_5V0			5.000
#define VREF_2V5			2.500

#define ADC_REF_G			3355.4428	// equ. [REG_MAX/mV] (10^-3 * 2^23-1)/(Vref); Vref=2.500V, g=9,80665m/s^2
#define ADC_REF_MG			3.3554428	// equ. [REG_MAX/uV] (10^-6 * 2^23-1)/(Vref)
#define ADC_REF_MS2			342.15995	// equ. [REG_MAX/mV * g/ms^-2] (10^-3 * 2^23-1)/(Vref * |g|)
#define ADC_REF_MMS2		0.34215995	// equ. [REG_MAX/mV * g/mms^-2] (10^-6 * 2^23-1)/(Vref * |g|)
#define ADC_REF_V 			3355442.8	// equ. [REG_MAX/V] (2^23-1)/(Vref)
#define ADC_REF_MV 			3355.4428	// equ. [REG_MAX/mV] (10^-3 * 2^23-1)/(Vref)
#define ADC_REF_UV 			3.3554428	// equ. [REG_MAX/uV] (10^-6 * 2^23-1)/(Vref)
// a [g] = (REG + OFFSET) / (ADC_REF_G * Sg * G)
// a [mg] = (REG + OFFSET) / (ADC_REF_MG * Sg * G)
// a [m/s^2] = (REG + OFFSET) / (ADC_REF_MS2 * Sg * G)
// a [mm/s^2] = (REG + OFFSET) / (ADC_REF_MMS2 * Sg * G)
// a [V] = (REG + OFFSET) / (ADC_REF_V * G)
// a [mV] = (REG + OFFSET) / (ADC_REF_MV * G)


// ****************************************************** //
// ******************** CONSTANTS SG ******************** //
// ****************************************************** //

#define AD_mV_V				16777.215		// (2^24-1)/10^3  (tens.)
#define AD_um_m				16.777215		// (2^24-1)/10^6  (tens.)

#define a_N					{1,2,5,10,20,50,100,250,500}


// ****************************************************** //
// ********************** CARD TYPE ********************* //
// ****************************************************** //

#define CARD_WIVID_V1			0x601
#define CARD_WIVID_V2			0x602

#define CARD_SIBOX_CLPS_V1		0x701		// measurement card
#define CARD_SIBOX_CLPS_MON_V1	0x711		// measurement card + monitor

#define CARD_DIBOX_CLPS_MON_V1	0x741		// measurement card + monitor

#define CARD_EXBOX_CLPS_MON_V1	0x761		// measurement card + monitor

#define CARD_SIBOX_SG_V1		0x801

#define CARD_KP_KOZ_V4			0x402
#define CARD_VIMEA_V1			0x403

#define CARD_STRETTON_V3		0xA03
#define CARD_STRETTON_V4		0xA04

#define CARD_MVIBE_V2			0x603
#define CARD_MVIBE_V3			0x604

//// ****************************************************** //
//// ********************** CARD TYPE ********************* //
//// ****************************************************** //
//
//#define DEV_TCP_PORT			5000	// for setting device TCP server port
//#define DEV_UDP_PORT			5000	// for setting device UDP port


#endif /* PROTOCOL_V3_H_ */
