/*
 * MAX17055.c
 *
 *  Created on: 6 gru 2017
 *      Author: Koperek
 */

#include <stdio.h>
#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>
#include "board_mvibe.h"
#include "max17055.h"

char 							tag1[] = "MAX";

static esp_err_t writeRegister( uint8_t reg, uint8_t data_0, uint8_t data_1 )
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, ( FG_I2C_ADDRESS << 1 ) | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, data_0, ACK_VAL);
	i2c_master_write_byte(cmd, data_1, NACK_VAL);
	i2c_master_stop(cmd);
	int ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 100/portTICK_PERIOD_MS);
	i2c_cmd_link_delete(cmd);

	if (ret != ESP_OK)
	{
		ESP_LOGE( tag1, "i2c_error_write reg: %d\n",ret);
	}
	return ret;
}

int readRegister( uint8_t reg, uint16_t *data_out)
{
	uint8_t data[2];

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 0, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 1, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, &data[0], ACK_VAL);
	i2c_master_read_byte(cmd, &data[1], NACK_VAL);

	i2c_master_stop(cmd);
	int ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 100/portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);


	*data_out = (int16_t)((data[1] << 8) | data[0]);

	if (ret != ESP_OK)
		ESP_LOGE( tag1, "i2c_error_write reg: %d\n",ret);

	return ret;
}

int readRegister_ver( uint8_t reg, uint8_t *data_0, uint8_t *data_1)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 0, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 1, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, data_0, ACK_VAL);
	i2c_master_read_byte(cmd, data_1, NACK_VAL);

	i2c_master_stop(cmd);
	int ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 100/portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

	if (ret != ESP_OK)
		ESP_LOGE( tag1, "i2c_error_write reg: %d\n",ret);

	return ret;
}

int max17055_read(fuel_gaguge_t *FuelRe )
{
	uint8_t data[24];

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 0, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x05, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, FG_I2C_ADDRESS << 1 | 1, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, &data[0], ACK_VAL);
	i2c_master_read_byte(cmd, &data[1], ACK_VAL);

	i2c_master_read_byte(cmd, &data[2], ACK_VAL);
	i2c_master_read_byte(cmd, &data[3], ACK_VAL);

	i2c_master_read_byte(cmd, &data[4], ACK_VAL);
	i2c_master_read_byte(cmd, &data[5], ACK_VAL);

	i2c_master_read_byte(cmd, &data[6], ACK_VAL);
	i2c_master_read_byte(cmd, &data[7], ACK_VAL);

	i2c_master_read_byte(cmd, &data[8], ACK_VAL);
	i2c_master_read_byte(cmd, &data[9], ACK_VAL);

	i2c_master_read_byte(cmd, &data[10], ACK_VAL);
	i2c_master_read_byte(cmd, &data[11], ACK_VAL);

	i2c_master_read_byte(cmd, &data[12], ACK_VAL);
	i2c_master_read_byte(cmd, &data[13], ACK_VAL);

	i2c_master_read_byte(cmd, &data[14], ACK_VAL);
	i2c_master_read_byte(cmd, &data[15], ACK_VAL);

	i2c_master_read_byte(cmd, &data[16], ACK_VAL);
	i2c_master_read_byte(cmd, &data[17], ACK_VAL);

	i2c_master_read_byte(cmd, &data[18], ACK_VAL);
	i2c_master_read_byte(cmd, &data[19], ACK_VAL);

	i2c_master_read_byte(cmd, &data[20], ACK_VAL);
	i2c_master_read_byte(cmd, &data[21], ACK_VAL);

	i2c_master_read_byte(cmd, &data[22], ACK_VAL);
	i2c_master_read_byte(cmd, &data[23], NACK_VAL);


	i2c_master_stop(cmd);
	int ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 100/portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);


	FuelRe->RAAC = (int16_t)(( data[ 1 ] << 8 ) | data[ 0 ] );
	FuelRe->RARC = (int16_t)(( data[ 3 ] ));
	FuelRe->BatteryTemperature= (int16_t)((( data[ 7 ] << 8 ) | data[ 6 ] ) / 256 );
	FuelRe->BatteryVoltage = (int16_t)(((( data[ 9 ] << 8 ) | data[ 8 ] ) / 16 ) * 1.25 );
	FuelRe->BatteryCurrent = (int16_t)((( data[ 11 ] << 8 ) | data[ 10 ] ) * 0.1562 );
	FuelRe->FullCapacity = (int16_t)((( data[ 23 ] << 8 ) | data[ 22 ] ));

	if (ret != ESP_OK)
		ESP_LOGE( tag1, "i2c_error_write reg: %d\n",ret);

	return ret;
}

void WriteAndVerifyRegister( uint8_t reg, uint8_t data_0, uint8_t data_1 )
{
	uint8_t data_0r, data_1r;
	int Attempt = 0;
	do {
		 writeRegister( reg, data_0, data_1 );
		 vTaskDelay(1); //1ms
		 readRegister_ver( reg, &data_0r, &data_1r );
	}
	while ( data_0 != data_0r && data_1 != data_1r && Attempt++<3 );
}


void max17055_init(device_register_t DevReg)
{
	uint16_t StatusPOR;

	do
	{
		readRegister(0x00, &StatusPOR);
		StatusPOR = StatusPOR & 0x0002;

		if (StatusPOR != 0)
		{
			uint16_t DNR;
			readRegister(0x3D, &DNR);
			DNR = DNR & 1;

			while(DNR)
			{
				vTaskDelay(10);
				readRegister(0x3D, &DNR);
				DNR = DNR & 1;
			}

			uint16_t HibCFG;

			writeRegister (0x18 , 0xB0, 0x04) ; // Write DesignCap
			writeRegister (0x45 , 0x36, 0x00) ; //Write dQAcc
			writeRegister (0x1E , 0x40, 0x06) ; ; // Write IchgTerm
			writeRegister (0x3A , 0x61, 0xA5) ; // Write VEmpty
			readRegister(0xBA, &HibCFG) ; //Store original HibCFG value
			writeRegister (0x60 , 0x90, 0x00) ; // Exit Hibernate Mode step 1
			writeRegister (0xBA , 0x00, 0x00) ; // Exit Hibernate Mode step 2
			writeRegister (0x60 , 0x00, 0x00) ; // Exit Hibernate Mode step 3
			writeRegister (0x46 , 0x90, 0x01) ; //Write dPAcc
			writeRegister (0xDB , 0x00, 0x80) ; //Write ModelCFG
			writeRegister (0x38 , 0x4d, 0x00) ; // Write RCOMP0
			writeRegister (0x39 , 0x32,0x2e) ; // Write TempCo
			writeRegister (0x12 , 0x50, 0x10) ; // Write QRTable00
			writeRegister (0x22 , 0x12, 0x20) ; // Write QRTable10
			uint8_t a= HibCFG & 0xff;
			uint8_t b= HibCFG >> 8;

			writeRegister (0xBA , b,a) ; // Restore Original HibCFG value
		}

		uint16_t Status;
		readRegister(0x00, &Status); //Read Status

		uint8_t a= Status & 0xff;
		uint8_t b= Status >> 8;

		Status = Status & 0xFFFD;

		WriteAndVerifyRegister (0x00, b, a ) ; //Write and Verify Status with POR bit cleared

		readRegister(0x00, &StatusPOR);
		StatusPOR = StatusPOR  & 0x0002;

	} while(StatusPOR != 0);

/// could be unnecessarily

	uint16_t a= DevReg.Saved_RCOMP0 & 0xff;
	uint16_t b= DevReg.Saved_RCOMP0 >> 8;
	WriteAndVerifyRegister(0x38, b, a) ;

	a= DevReg.Saved_TempCo & 0xff;
	b= DevReg.Saved_TempCo >> 8;
	WriteAndVerifyRegister(0x39, b, a) ;

	a= DevReg.Saved_FullCapNom & 0xff;
	b= DevReg.Saved_FullCapNom >> 8;
	WriteAndVerifyRegister(0x23, b, a) ;
	vTaskDelay(350 / portTICK_PERIOD_MS);

	///////////////////////

	uint16_t Data;
	readRegister(0x2B, &Data) ; //Read MiscCFG
	Data |= 0x1400 ; //Set bits 10 and 12
	a= Data & 0xff;
	b= Data >> 8;

	writeRegister (0x2B, b,a) ; //Write MiscCFG

	readRegister(0x2B, &Data) ; //Read MiscCFG
	a= Data & 0xff;
	b= Data >> 8;

	writeRegister (0x2B, b,a) ; //Write MiscCFG

	vTaskDelay(500 / portTICK_PERIOD_MS);

	writeRegister (0x18, 0xB0, 0x04);
}
