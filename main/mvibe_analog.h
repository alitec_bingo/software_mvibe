/*
 * mvibe_analog.h
 *
 * Created: 2015-08-17 14:43:03
 *  Author: mmakow
 */

#include "device_mvibe_db.h"
#include <esp_system.h>
#include <stdbool.h>

#ifndef MVIBE_ANALOG_H_
#define MVIBE_ANALOG_H_

/* ---------------------------------------------------------------------------------------- */

#define WLAN_SSID_LEN		32


#define I2S_BCK						4
#define I2S_LRCK					2
#define I2S_DATA_OUT_IN				0

#define I2S_NUM         			(0)
#define I2S_NUM_1         			(1)
#define I2S_BLOCK_SIZE				( 256 )

#define I2S_BUFFER_SIZE				512
#define I2S_DMA_LEN					512
#define I2S_DMA_COUNT				2

#define NOCH_LSM					6
#define NOSA_LSM					256
#define PACK_OF_ONE_CH				256
#define LSM_FIFO_WM_SIZE			( NOCH_LSM * NOSA_LSM )

uint32_t buffer_i2s[ I2S_BUFFER_SIZE ];
uint32_t buffer_i2s_send[ I2S_BUFFER_SIZE ];

int buffer_lsm[ 512 ];
int buffer[ 2048 ];


/* ---------------------------------------------------------------------------------------- */

#define		TCC_1S_CONST						100
#define		DEF_PWDN_NO_TIMEOUT					( 1000000 * TCC_1S_CONST )	// [s]
#define		DEF_PWDN_15MIN_TIMEOUT				( 900 * TCC_1S_CONST )		// [s]
#define		DEF_PWDN_5MIN_TIMEOUT				( 300 * TCC_1S_CONST )		// [s]

/* ---------------------------------------------------------------------------------------- */
typedef struct _device_register_t
{
	volatile uint32_t	WriteCounter;
	volatile uint16_t	RefreshAfterFlashing;

	uint32_t	CardType;
	uint32_t	SerialNumber_WiFi[ 6 ];
	char		SSID_WiFi[ WLAN_SSID_LEN ];
	char		Password_Wifi[ WLAN_SSID_LEN ];
	uint32_t	PowerDownTimeout;

	uint8_t		Trigger;
	uint32_t	SampleFrequency;
	uint32_t	NumberOfSamples;
	uint8_t		NumberOfChannels;

	uint8_t		ChType[MAX_NOCH];
	uint8_t		ChSequence[MAX_NOCH];
	float		ChRangeMin[MAX_NOCH];
	float		ChRangeMax[MAX_NOCH];
	uint8_t		ChRangeIdx[MAX_NOCH];
	float		ChSensorSens[MAX_NOCH];			// Sensor Sensitivity [mV/g]
	float		ChCalGainInt[NOVR][MAX_NOCH];	// Channel Gain	[V/V] w. internal sensor
	float		ChCalOffsetInt[NOVR][MAX_NOCH];	// Channel Offset [V] w. internal sensor
	float		ChCalGainExt[NOVR][MAX_NOCH];	// Channel Gain	[V/V] w. external sensor
	float		ChCalOffsetExt[NOVR][MAX_NOCH];	// Channel Offset [V] w. external sensor
	uint8_t		ChUnit[MAX_NOCH];				// 0 - raw data; 1 - uV; 2 - mV; 3 - V; 5 - mg; 6 - g; 7 - mm/s2; 8 - m/s2
	uint8_t		ChSensorType[MAX_NOCH];			// 0 - digital; 1 - AC I src;
	uint8_t		ChSensorTypeSequence[MAX_NOCH];

	uint8_t		SensorIntExtSw;					// 0: N/A; 1: Internal Sensors [Int_X, Int_Y, Int_Z, Ext_X]; 2: External Sensors [Ext_X, Ext_Y, Ext_Z, Int_X]
	uint8_t		Fram_version;

	uint16_t Saved_RCOMP0;
	uint16_t Saved_TempCo ;
	uint16_t Saved_FullCapRep;
	uint16_t Saved_Cycles ;
	uint16_t Saved_FullCapNom;

	int			tpThreshold;



} device_register_t;

typedef struct _device_state_t
{
	// device additional registers
	uint16_t				ChPgaRegister[MAX_ADC];
	float 					ChAccelUnity[MAX_NOCH];			// ADC Register Value

	// communication registers
	remote_measure_state_t 	MeasurementRemoteState;
	config_state_t			ConfigState;

	remote_measure_mode_t	MeasurementMode;
	remote_measure_param_t	MeasurementParam;

	uint32_t 	PackIndex;
	uint32_t	PackOfSamples;
	uint32_t 	NumberOfPackToSend;
	uint32_t 	PackSizeHeader;
	uint32_t 	PackSizeSample;

	//	uint32_t 	NumberOfMeasurements;
	//	uint32_t	MeasurementPeriod;
	//	uint16_t	PeriodicMeasurmentCnt;
	//	uint16_t	TimeMarker[6];

} device_state_t;


typedef struct _device_acquire_t
{
	volatile uint32_t 	SamplesCntSent;				// counter of sent samples
	volatile bool		HeaderBufferSent;

} device_acquire_t;

typedef enum
{
	ADC_IDLE,
	ADC_STOPPED,
	ADC_START

} adc_state_t;


typedef struct _sensor_sens_t
{

	float	sens_z;
	float	sens_x;
	float	sens_acce;
	float	sens_gyro;
	float	Sensor_Sens[NOSENSORS];
	int		serial_no;
	uint32_t button_time;
	uint32_t button_sleep;

}sensor_sens_t;

typedef struct _flags_wraper_led_t
{

	bool flag;
	bool mic_flag;
	bool no_mems_flag;
	bool measure_flag;
	int err_wraper;

}flags_wraper_led_t;

/* ---------------------------------------------------------------------------------------- */

void calculate_unit_bbox_ver_1( uint8_t idx );

void i2s_init_RIGHT();
void i2s_init_LEFT();
void i2s_init_DUAL();
void i2s_mesaurement();

void i2c_config();
void pcm_config();
esp_err_t pcm_wirte_reg( uint8_t reg, uint8_t data);
void sleep_pcm();

#endif /* MVIBE_ANALOG_H_ */
