/*
 * mvibe_analog.c
 *
 *  Created on: 16 sie 2017
 *      Author: admin
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <esp_intr.h>
#include <soc/i2s_reg.h>
#include <esp_log.h>
#include <driver/gpio.h>
#include <driver/i2c.h>
#include <driver/i2s.h>
#include "mvibe_analog.h"
#include "pcm186x.h"
#include "LSM6DSMTR.h"

static const char *TAG = "Analog";

extern device_register_t			DevReg;				// Device Main Register
extern device_state_t				DevState;			// Device State Register

extern flags_wraper_led_t			Flags_Wraper;

/* ---------------------------------------------------------------------------------------- */
//

void i2s_init_DUAL()
{
	i2s_driver_uninstall( I2S_NUM );

	i2s_config_t i2s_config_rx =
	{
		mode: (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
		sample_rate: DevReg.SampleFrequency,
		bits_per_sample: I2S_BITS_PER_SAMPLE_32BIT,
		channel_format: I2S_CHANNEL_FMT_RIGHT_LEFT,
		communication_format: (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB ),
		intr_alloc_flags: ESP_INTR_FLAG_LEVEL1,
		dma_buf_count: I2S_DMA_COUNT,
		dma_buf_len: I2S_DMA_LEN,
		use_apll: 1
	};

	i2s_pin_config_t pin_config =
	{
		.bck_io_num = I2S_BCK,
		.ws_io_num = I2S_LRCK,
		.data_out_num = I2S_PIN_NO_CHANGE,
		.data_in_num = I2S_DATA_OUT_IN
	};

	i2s_driver_install( I2S_NUM, &i2s_config_rx, 0, NULL );
	i2s_set_pin( I2S_NUM, &pin_config );
}

void i2s_init_DEN()
{
	i2s_driver_uninstall( I2S_NUM );

	i2s_config_t i2s_config_rx =
	{
		mode: (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
		sample_rate: 8192,
		bits_per_sample: I2S_BITS_PER_SAMPLE_32BIT,
		channel_format: I2S_CHANNEL_FMT_RIGHT_LEFT,
		communication_format: (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB ),
		intr_alloc_flags: ESP_INTR_FLAG_LEVEL1,
		dma_buf_count: I2S_DMA_COUNT,
		dma_buf_len: I2S_DMA_LEN,
		use_apll: 1
	};

	i2s_pin_config_t pin_config =
	{
		.bck_io_num = I2S_BCK,
		.ws_io_num = I2S_LRCK,
		.ws_io_num_den = GPIO_NUM_33,
		.data_out_num = I2S_PIN_NO_CHANGE,
		.data_in_num = I2S_DATA_OUT_IN
	};
	i2s_driver_install( I2S_NUM, &i2s_config_rx, 0, NULL );
	i2s_set_pin_den( I2S_NUM, &pin_config );

	ESP_LOGI(TAG,"\n\n DEN INIT \n\n");
}
void i2s_mesaurement( char * buff, unsigned int len )
{
	i2s_read_bytes(I2S_NUM, buff, len, portMAX_DELAY);
}

void calculate_unit_bbox_ver_1( uint8_t idx )
{
	DevState.ChAccelUnity[idx] = 1;

	if (DevReg.ChUnit[idx] == ADS_UNIT_uV)
		{DevState.ChAccelUnity[idx] = ADC_REF_UV;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_mV)
		{DevState.ChAccelUnity[idx] = ADC_REF_MV;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_V)
		{DevState.ChAccelUnity[idx] = ADC_REF_V;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_mg)
		{DevState.ChAccelUnity[idx] = DevReg.ChSensorSens[idx] * ADC_REF_MG;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_g)
		{DevState.ChAccelUnity[idx] = DevReg.ChSensorSens[idx] * ADC_REF_G;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_mms2)
		{DevState.ChAccelUnity[idx] = DevReg.ChSensorSens[idx] * ADC_REF_MMS2;}
	if (DevReg.ChUnit[idx] == ADS_UNIT_ms2)
		{DevState.ChAccelUnity[idx] = DevReg.ChSensorSens[idx] * ADC_REF_MS2;}

	DevState.ChAccelUnity[idx] = 1 / DevState.ChAccelUnity[idx];
}

/* ---------------------------------------------------------------------------------------- */

void i2c_config()
{
	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = SDA_PIN;
	conf.scl_io_num = SCL_PIN;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = I2C_EXAMPLE_MASTER_FREQ_HZ;
	i2c_param_config(I2C_NUM_0, &conf);
	i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0);
}

esp_err_t pcm_wirte_reg( uint8_t reg, uint8_t data)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, ( PCM_I2C_ADDRESS << 1 ) | WRITE_BIT, 1);
	i2c_master_write_byte(cmd, reg, 1);
	i2c_master_write_byte(cmd, data, 1);
	i2c_master_stop(cmd);
	int ret = 	i2c_master_cmd_begin(I2C_NUM_0, cmd, 100/portTICK_PERIOD_MS);
	i2c_cmd_link_delete(cmd);

	if (ret != ESP_OK)
	{
		ESP_LOGE(TAG, "Write error = %d", ret);
		return ret;
	}
	return ESP_OK;
}

static esp_err_t pcm_read_reg( uint8_t reg, uint8_t *data)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, ( PCM_I2C_ADDRESS << 1 ) | WRITE_BIT, 1);
	i2c_master_write_byte(cmd, reg, 1);

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, ( PCM_I2C_ADDRESS << 1 ) | READ_BIT, 1);
	i2c_master_read_byte(cmd, data, 1);
	i2c_master_stop(cmd);

	int ret = 	i2c_master_cmd_begin(I2C_NUM_0, cmd, 200/portTICK_PERIOD_MS);
	i2c_cmd_link_delete(cmd);

	if (ret != ESP_OK)
	{
		ESP_LOGE(TAG, "Write error = %d", ret);
		return ret;
	}
	return ESP_OK;
}

void pcm_config()
{
	bool piezo_flag = 0;
	uint8_t value = 0;

	Flags_Wraper.no_mems_flag = 1;
	for(int i = 0; i < DevReg.NumberOfChannels; i++)
	{
		if( DevReg.ChSequence[ i ] >= 3)
			Flags_Wraper.no_mems_flag = 0;

		if( DevReg.ChSequence[ i ] == 0 || DevReg.ChSequence[ i ] == 1)
			piezo_flag = 1;

		if( DevReg.ChSequence[ i ] == 2)
			Flags_Wraper.mic_flag = 1;

		if( piezo_flag && Flags_Wraper.mic_flag)
		{
			Flags_Wraper.err_wraper = 1;
			Flags_Wraper.mic_flag = 0;
		}

	}

	if( Flags_Wraper.no_mems_flag == 1 )
	{
			ESP_LOGI(TAG,"\n KONFIGURACJA \n");

			pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, PCM186X_REG_RESET );		//Resetowanie PCM

			pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, 0x03 );						// select Page3

			pcm_wirte_reg( PCM186X_REG_GPIO_1_0_DIR, 0x40 );

			pcm_wirte_reg( PCM186X_REG_MIC_BIAS_CTRL, 0x00 );					// 0x15

			pcm_wirte_reg( PCM186X_REG_PAGE_SELECT, 0x00 );						// select Page0
			pcm_wirte_reg( PCM186X_REG_PWDOWN_CONF, 0b01110001 );				// 0x70 - standby

			pcm_wirte_reg( PCM186X_REG_ADC1L_IN_SEL, 0b01000010 );				// 0x06 - VIN1L2
			pcm_wirte_reg( PCM186X_REG_ADC1R_IN_SEL, 0b01000010 );				// 0x07 - VIN1R2
			pcm_wirte_reg( PCM186X_REG_ADC2L_IN_SEL, 0b01000000 );				// 0x08 - none
			pcm_wirte_reg( PCM186X_REG_ADC2R_IN_SEL, 0b01000000 );				// 0x09 - none

			pcm_wirte_reg( PCM186X_REG_GPIO_1_0_FUNC, 0b00010001 );				// 0x10 - GPIO0 and GPIO1 as DIGMIC_IN0 and DIGMIC_IN1
			pcm_wirte_reg( PCM186X_REG_GPIO_3_2_FUNC, 0b00000001 );				// 0x11 - GPIO2 as DIGMIC_CLK
			pcm_wirte_reg( PCM186X_REG_GPIO_PULLDOWN, 0b00000000 );				// 0x15

			//pcm_wirte_reg( PCM186X_REG_DPGA_VAL_CH1_L, 0x00 );				// 0x0F DPGA gain = 0dB (default)
			//pcm_wirte_reg( PCM186X_REG_DPGA_VAL_CH1_R, 0x00 );				// 0x16 DPGA gain = 0dB (default)

			pcm_wirte_reg( PCM186X_REG_DPGA_CH_2_1_CTRL, 0xff );				// 0x19 - DPGA manual config (set 0dB)

			pcm_wirte_reg( PCM186X_REG_AUDIO_INTERFACE_FORMAT, 0b01000100 );	// 0x0B
			pcm_wirte_reg( PCM186X_REG_TDM_OSEL, 0b00000000 );					// 0x0C
			pcm_wirte_reg( PCM186X_REG_TX_TDM_OFFSET, 0b00000001 );				// 0x0D
			pcm_wirte_reg( PCM186X_REG_RX_TDM_OFFSET, 0b00000001 );				// 0x0E

			pcm_wirte_reg( PCM186X_REG_DIN_RESAMP, 0b00000001 );				// 0x1B
			pcm_wirte_reg( PCM186X_REG_CLOCK_CONF, 0b00101110 );				// 0x20

			pcm_wirte_reg( PCM186X_REG_PLL_FRAC_JD_DIV_LSB, 0 );				// 0x2C
			pcm_wirte_reg( PCM186X_REG_PLL_FRAC_JD_DIV_MSB, 0 );				// 0x2D

			pcm_wirte_reg( PCM186X_REG_INT_CONF1, 0x00 );						// 0x60

			pcm_wirte_reg( PCM186X_REG_DSP_CTRL, 0b10011100 );					// 0x71

			if ( piezo_flag )
			{
				i2s_init_DUAL();
				ESP_LOGI(TAG, "\n KONFIGURACJA TRYB ADC \n ");

				pcm_wirte_reg( PCM186X_REG_DIG_MIC_CONF, 0b01000000 );

				switch( DevReg.SampleFrequency )
				{
				case 8192:
					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 31 );			//21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 31 );			//22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  63 );			//23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	31 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 0 );					//29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 3 );					//2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			//2B
					break;

				case 16384:
					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 15 );			//21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 15 );			//22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  31 );			//23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	15 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					//29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 3 );					//2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			//2B
					break;

				case 32768:
					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 7 );				//21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 7 );				//22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  15 );			//23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	7 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					//29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					//2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			//2B
					break;

				case 65536:
					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 3 );				//21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 3 );				//22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  7 );			//23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	3 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					//29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					//2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 16 );			//2B
					break;

				case 131072:
					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 1 );				//21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 1 );				//22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  7 );			//23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	1 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					//29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					//2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 8 );				//2B
					break;
				}
			}

			// PDM configurations

			if ( Flags_Wraper.mic_flag )
			{
				i2s_init_DUAL();
				ESP_LOGI(TAG, "\n KONFIGURACJA TRYB PDM \n ");

				pcm_wirte_reg( PCM186X_REG_DIG_MIC_CONF, 0b01000001 );

				switch( DevReg.SampleFrequency )
				{
				case 8192:

					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 		15 );		// 0x21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 		15 );		// 0x22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  		15 );		// 0x23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	31 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 0 );					// 0x29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 3 );					// 0x2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32);				// 0x2B
					break;

				case 16384:

					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 		7 );		// 0x21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 		7 );		// 0x22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  		7 );		// 0x23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	15 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					// 0x29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 3 );					// 0x2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			// 0x2B
					break;

				case 32768:

					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 		3 );		// 0x21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 		3 );		// 0x22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  		7 );		// 0x23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	7 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					// 0x29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					// 0x2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 32 );			// 0x2B
					break;

				case 65536:

					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 		1 );		// 0x21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 		1 );		// 0x22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  		7 );		// 0x23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	3 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					// 0x29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					// 0x2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 16 );			// 0x2B
					break;

				case 131072:

					pcm_wirte_reg( PCM186X_REG_DSP1_CLOCK_DIV, 		0 );		// 0x21
					pcm_wirte_reg( PCM186X_REG_DSP2_CLOCK_DIV, 		0 );		// 0x22
					pcm_wirte_reg( PCM186X_REG_ADC_CLOCK_DIV,  		7 );		// 0x23

					pcm_wirte_reg( PCM186X_REG_PLL_SCK_CLOCK_DIV, 	1 );		// 0x25
					pcm_wirte_reg( PCM186X_REG_MCLK_CLOCK_DIV, 		3 );		// 0x26
					pcm_wirte_reg( PCM186X_REG_MSCK_CLOCK_DIV, 		63 );		// 0x27

					pcm_wirte_reg( PCM186X_REG_PLL_P_DIV, 1 );					// 0x29
					pcm_wirte_reg( PCM186X_REG_PLL_R_DIV, 1 );					// 0x2A
					pcm_wirte_reg( PCM186X_REG_PLL_INT_JD_DIV, 8 );				// 0x2B
					break;
				}
			}

			pcm_wirte_reg( PCM186X_REG_PLL_CONF, 0b00000011 );					// 0x28 - start PLL
			//	vTaskDelay( 50 );
			pcm_wirte_reg( PCM186X_REG_PWDOWN_CONF, 0b01110000 );				// 0x70 - run
			//
			pcm_read_reg( PCM186X_REG_PLL_CONF, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_PLL_CONF (%x) = 0x%02x\n", PCM186X_REG_PLL_CONF, value);

			pcm_read_reg( PCM186X_REG_DEVICE_STATE, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_DEVICE_STATE (%x) = 0x%02x\n", PCM186X_REG_DEVICE_STATE, value);

			pcm_read_reg( PCM186X_REG_DEVICE_INFO, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_DEVICE_INFO (%x) = 0x%02x\n", PCM186X_REG_DEVICE_INFO, value);

			pcm_read_reg( PCM186X_REG_CLK_RATIO_INFO, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_CLK_RATIO_INFO (%x) = 0x%02x\n", PCM186X_REG_CLK_RATIO_INFO, value);

			pcm_read_reg( PCM186X_REG_CLK_STATUS, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_CLK_STATUS (%x)  = 0x%02x\n", PCM186X_REG_CLK_STATUS, value);

			pcm_read_reg( PCM186X_REG_PWR_SUPPLY_STATUS, &value );
			ESP_LOGI(TAG," - wartosc rej.: PCM186X_REG_PWR_SUPPLY_STATUS (%x) = 0x%02x\n", PCM186X_REG_PWR_SUPPLY_STATUS, value);

			ESP_LOGI(TAG," - koniec\nKONFIGURACJA PCM1865\n\n");
	}
	else
	{

		if( DevReg.SampleFrequency != 8192)
		{
			Flags_Wraper.err_wraper = 2;
		}

		i2s_init_DEN();
		acce_set_reg();
	}
}

void sleep_pcm()
{
	pcm_wirte_reg(0x00, 0x00);
	pcm_wirte_reg(0x70, 0x14);
	pcm_wirte_reg(0x00, 0x03);
	pcm_wirte_reg(0x12, 0x41);
	pcm_wirte_reg(0x00, 0x00);


	ESP_LOGE("KURLA", "PCM SLEEP \n");
}
