/*
 * winc_app.h
 *
 * Created: 2015-04-10 09:28:33
 *  Author: mmakow
 */


#ifndef WINC_APP_H_
#define WINC_APP_H_

#include <sys/socket.h>
/* FreeRTOS event group to signal when we are connected to wifi*/
//extern EventGroupHandle_t tcp_event_group;

#define WIFI_CONNECTED_BIT 							BIT0

/* ---------------------------------------------------------------------------------------- */

#define MAX_SOCKET									4

#define	TCPIP_BUFF_SIZE								1460
#define	UDP_BUFF_SIZE								128

#define TCP_PACKET_MAX_LEN							1024

#define	MAX_TCP_BUFF_SIZE							( 2 * TCPIP_BUFF_SIZE )
#define	MAX_UDP_BUFF_SIZE							128

/* ---------------------------------------------------------------------------------------- */


typedef int SOCKET;

typedef struct
{
	SOCKET				listenSocketHdl;
	SOCKET				acceptedSocketHdl;
	struct sockaddr_in	socketAddr;

	bool				FlagCheckSocketTcp;
	int32_t				recvLen;

} my_sockets;

/* ---------------------------------------------------------------------------------------- */

uint8_t get_listen_socket_struct_idx( SOCKET sock );
uint8_t get_accepted_socket_struct_idx( SOCKET sock );

void reset_struct_mySockReg( int mySockIdx );
void struct_mySockReg_init( void );

void int_to_tab_ip( uint32_t ip, uint8_t *tab_ip );

#endif /* WINC_APP_H_ */
