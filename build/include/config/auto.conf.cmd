deps_config := \
	/home/admin/esp-idf-v3.0-rc1/components/app_trace/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/aws_iot/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/bt/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/esp32/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/ethernet/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/fatfs/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/freertos/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/heap/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/libsodium/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/log/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/lwip/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/mbedtls/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/openssl/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/pthread/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/spi_flash/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/spiffs/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/tcpip_adapter/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/wear_levelling/Kconfig \
	/home/admin/esp-idf-v3.0-rc1/components/bootloader/Kconfig.projbuild \
	/home/admin/esp-idf-v3.0-rc1/components/esptool_py/Kconfig.projbuild \
	/home/admin/esp-idf-v3.0-rc1/components/partition_table/Kconfig.projbuild \
	/home/admin/esp-idf-v3.0-rc1/Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
